cd html
rsync --verbose  --progress --stats --compress --rsh=/usr/bin/ssh \
      --recursive --times --perms --links --delete \
      --exclude "*bak" --exclude "*~" \
      home.html index.html set.html sets js css cs70@cory.eecs.berkeley.edu:public_html/fa14/online/
cd ..
