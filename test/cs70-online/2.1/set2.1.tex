
% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode

% This is a simple template for a LaTeX document using the "article" class.
% See "book", "report", "letter" for other types of document.

\documentclass[11pt, preview]{standalone} % use larger type; default would be 10pt

\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)


\usepackage{../../markup}
%%% Examples of Article customizations
% These packages are optional, depending whether you want the features they provide.
% See the LaTeX Companion or other references for full information.

%%% PAGE DIMENSIONS
\usepackage{geometry} % to change the page dimensions
\geometry{a4paper} % or letterpaper (US) or a5paper or....
% \geometry{margin=2in} % for example, change the margins to 2 inches all round
% \geometry{landscape} % set up the page for landscape
%   read geometry.pdf for detailed page layout information

\usepackage{graphicx} % support the \includegraphics command and options

% \usepackage[parfill]{parskip} % Activate to begin paragraphs with an empty line rather than an indent

%%% PACKAGES
\usepackage{amsmath, amsfonts,amssymb}
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{paralist} % very flexible & customisable lists (eg. enumerate/itemize, etc.)
\usepackage{verbatim} % adds environment for commenting out blocks of text & for better verbatim
\usepackage{subfig} % make it possible to include more than one captioned figure/table in a single float
% These packages are all incorporated in the memoir class to one degree or another...

%%% HEADERS & FOOTERS
\usepackage{fancyhdr} % This should be set AFTER setting up the page geometry
\pagestyle{fancy} % options: empty , plain , fancy
\renewcommand{\headrulewidth}{0pt} % customise the layout...
\lhead{}\chead{}\rhead{}
\lfoot{}\cfoot{\thepage}\rfoot{}

%%% SECTION TITLE APPEARANCE
\usepackage{sectsty}
\allsectionsfont{\sffamily\mdseries\upshape} % (See the fntguide.pdf for font help)
% (This matches ConTeXt defaults)

%%% ToC (table of contents) APPEARANCE
\usepackage[nottoc,notlof,notlot]{tocbibind} % Put the bibliography in the ToC
\usepackage[titles,subfigure]{tocloft} % Alter the style of the Table of Contents
\renewcommand{\cftsecfont}{\rmfamily\mdseries\upshape}
\renewcommand{\cftsecpagefont}{\rmfamily\mdseries\upshape} % No bold!

\newcommand{\N}{\mathbb{N}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\Q}{\mathbb{Q}}
%%% END Article customizations

%%% The "real" document content comes below...

%\date{} % Activate to display a given date or no date (if empty),
         % otherwise the current date is printed 

\begin{document}
\config{name}{The Well-Ordering Principle}
\noindent{\bf The Well-Ordering Principle}.

Match the following familiar inductive proofs with the equivalent proof sketch that uses the well-ordering principle.
\begin{enumerate}
\item \emph{Theorem.} For all $n \in \N$, $\sum_{i=0}^n i = \frac{n(n+1)}{2}$.

\emph{Proof}. 

{\bf Base Case:} Consider $n = 0$; then $\sum_{i=0}^0 i = 0 = \frac{0(0+1)}{2}$, so the base case holds.

{\bf Inductive Hypothesis:} Suppose that the statement holds true for $k \in \N$. 

{\bf Inductive Step:} Consider $k+1$. Then we have
\begin{eqnarray*}
\sum_{i=0}^{k+1} i &=& k+1 + \sum_{i=0}^{k} i\\
&=& k+1 + \frac{k(k+1)}{2} \qquad \text{(By the inductive hypothesis)}\\
&=& \frac{2(k+1) + k(k+1)}{2} \\
&=& \frac{(k+2)(k+1)}{2},
\end{eqnarray*}
as desired. Thus, the statement holds for $k+1$, and by induction our conclusion holds for all $n \in \N$. $\blacksquare$
\begin{enumerate}
\begin{Choices} 
\FalseChoice\item Suppose for contradiction that the statement is not true, and consider the smallest $n$ for which $\sum_{i=0}^n i \neq \frac{n(n+1)}{2}$. Then $\sum_{i=0}^{n-1} i = \frac{(n-1)n}{2}$. Adding $n$ to both sides, we get $\sum_{i=0}^n i = \frac{n(n+1)}{2}$, and obtain a contradiction. 
\TrueChoice\item Suppose for contradiction that the statement is not true, and consider the smallest $n$ for which $\sum_{i=0}^n i \neq \frac{n(n+1)}{2}$.  If $n=0$ this says $0 \neq 0$, contradiction. Otherwise we can subtract $n$ from both sides, to get $$\sum_{i=0}^{n-1} i \neq \frac{n(n+1)}{2} - n = \frac{n(n-1)}{2}$$, and so the statement does not hold for $n-1$, which contradicts the assumption that $n$ was the smallest value for which the statement did not hold.
\FalseChoice\item  Knowing that the statement is true for $n=0$, we suppose that the statement is not true in general, and we choose the largest $n$ for which $\sum_{i=0}^n i \neq \frac{n(n+1)}{2}$. Then we can add $n+1$ to both sides, but $\frac{n(n+1)}{2} + n+1 = \frac{(n+2)(n+1)}{2}$, and so the statement does not hold for $n+1$, which is a contradiction. 
\Hint What is the equivalent of the base case of the induction in a well-ordering principle based proof? Review the well-ordering principle, found in note 3.
\end{Choices}
\end{enumerate}

%--------------------------------------------------------------------------------
\item \emph{Theorem.} For all $n \in \N$, $n^3 - n$ is divisible by 3.

\emph{Proof}. 

{\bf Base Case:} Consider $n = 0$; then $n^3 - n = 0$ is divisible by 3, so the base case holds.

{\bf Inductive Hypothesis:} Suppose that the statement holds true for $k \in \N$. 

{\bf Inductive Step:} Consider $k+1$. Then we have
\begin{eqnarray*}
(k+1)^3 - k - 1 &=& k^3 + 3k^2 + 3k + 1 - k - 1\\
&=& (k^3 - k) + 3(k^2 + k).
\end{eqnarray*}
By the inductive hypothesis 3 divides the first term, and it clearly divides the second term. Thus, the statement holds for $k+1$, and by induction our conclusion holds for all $n \in \N$. $\blacksquare$
\begin{enumerate}
\begin{Choices}
\FalseChoice\item Assume the statement is not true, and choose the smallest $n$ for which it is false. Then, we can re-write the expression in terms of $k = n-1$, and we get that $n^3 - n = (k+1)^3 - (k+1) = (k^3 - k) + 3(k^2 +k)$. But since 3 does not divide this expression, then it must not divide the first term since it clearly divides the second, and since $k = n-1$ this is a contradiction.
\FalseChoice\item The statement is true for $n=0$. Now, we assume the statement is not true in general, and choose the largest $n$ for which it is false. Then we can use an argument similar to the one we used in the inductive step to show that $(n+1)^3 - (n+1) = (n^3 - n) + 3(n^2 + n)$, which is also not divisible by 3, a contradiction.
\TrueChoice\item The statement is true for $n=0$. Now, we assume the statement is not true in general, and let $n$ be the smallest value for which it is false. Then, we can re-write the expression in terms of $k = n-1$, and we get that $n^3 - n = (k+1)^3 - (k+1) = (k^3 - k) + 3(k^2 +k)$. But now $k^3 - k$ cannot be divisible by $3$, since it is the difference between the left hand side, which by assumption is not divisible by $3$, and $3(k^2 +k)$, which is. Since $k = n-1$ this contradicts that $n$ was the smallest value for which the statement did not hold. 
\Hint How does the base case work in a well-ordering principle-based proof? 
\end{Choices}
\end{enumerate}

%--------------------------------------------------------------------------------
\item \emph{Theorem.} For every list of $n \ge 1$ integers $L$ such that the sum of integers in $L$ sums to 0 $\left(\sum_{i \in L} i = 0\right)$, there exists at least one integer $x \in L$ such that $x \le 0$. 

\emph{Proof}. We proceed by induction on $n$, and furthermore strengthen the hypothesis to say that if $\sum_{i \in L} \le 0$ then there exists at least one integer $x \in L$ such that $x \le 0$. 

{\bf Base Case:} Consider $n = 1$; the list $L$ contains only one element, and since this element is the only one in the sum it is at most 0, as desired. 

{\bf Inductive Hypothesis:} Suppose that the statement holds true for lists of size $k$. 

{\bf Inductive Step:} Consider a list $L$ of size $k+1$. If there is no strictly positive element in $L$, then we are done. Otherwise, remove a positive element $p$ from $L$ to obtain a list $L'$ with $k$ elements. By assumption,
$$\sum_{i \in L'} i  = \sum_{i \in L} i - p \le 0 - p.$$
Now, we can apply the inductive hypothesis to $L'$ to realize that $L'$ must contain at least one non-positive element, and therefore so does $L$. Thus, the statement holds for $k+1$, and by induction our conclusion holds for all $n \in \N$. $\blacksquare$
\begin{enumerate}
\begin{Choices}
\TrueChoice\item We know the statement is true for sets of size $1$. Now, we assume the statement is false in general, and consider the smallest list size $n$ for which it is not true. Then there exists a list $L$ of $n$ elements which are all strictly positive, yet sum to a non-positive number. We choose one such positive element to remove from $L$ to obtain a list $L'$; since the element was positive the sum can only decrease, and therefore $L'$ is a list of $n-1$ strictly positive elements with a non-positive sum, and we have a contradiction. 
\FalseChoice\item Assume the statement is false, and consider the smallest list size $n$ for which it is not true. Let $L$ be such a list. We should be able to obtain $L$ by adding one element to any of its sub-lists of size $n-1$. Since each of these sub-lists must also have a non-positive sum (since we exclude a positive element to make it), they must contain a non-positive element, and we obtain a contradiction.  
\FalseChoice\item The statement holds for sets of size $1$. Now, we assume the statement is false in general, and consider the largest list size $n$ for which it is not true. Let $L$ be such a list with a negative sum, $\sum_{i \in L} i = -c < 0$. Then we can add $c$ to $L$ to obtain a list of size $n+1$ with a non-positive sum and all positive elements, which gives us a contradiction. 
\Hint Review the well-ordering principle, found in note 3.
\end{Choices}
\end{enumerate}
\end{enumerate}

\end{document}
