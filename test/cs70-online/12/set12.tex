% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode

% This is a simple template for a LaTeX document using the "article" class.
% See "book", "report", "letter" for other types of document.

\documentclass[11pt, preview]{standalone} % use larger type; default would be 10pt

\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)


\usepackage{../../markup}
%%% Examples of Article customizations
% These packages are optional, depending whether you want the features they provide.
% See the LaTeX Companion or other references for full information.

%%% PAGE DIMENSIONS
\usepackage{geometry} % to change the page dimensions
\geometry{a4paper} % or letterpaper (US) or a5paper or....
% \geometry{margin=2in} % for example, change the margins to 2 inches all round
% \geometry{landscape} % set up the page for landscape
%   read geometry.pdf for detailed page layout information

\usepackage{graphicx} % support the \includegraphics command and options
\usepackage{color, tikz}
% \usepackage[parfill]{parskip} % Activate to begin paragraphs with an empty line rather than an indent

%%% PACKAGES
\usepackage{amsmath, amsfonts,amssymb}
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{paralist} % very flexible & customisable lists (eg. enumerate/itemize, etc.)
\usepackage{verbatim} % adds environment for commenting out blocks of text & for better verbatim
\usepackage{subfig} % make it possible to include more than one captioned figure/table in a single float
% These packages are all incorporated in the memoir class to one degree or another...

%%% HEADERS & FOOTERS
\usepackage{fancyhdr} % This should be set AFTER setting up the page geometry
\pagestyle{fancy} % options: empty , plain , fancy
\renewcommand{\headrulewidth}{0pt} % customise the layout...
\lhead{}\chead{}\rhead{}
\lfoot{}\cfoot{\thepage}\rfoot{}

%%% SECTION TITLE APPEARANCE
\usepackage{sectsty}
\allsectionsfont{\sffamily\mdseries\upshape} % (See the fntguide.pdf for font help)
% (This matches ConTeXt defaults)

%%% ToC (table of contents) APPEARANCE
\usepackage[nottoc,notlof,notlot]{tocbibind} % Put the bibliography in the ToC
\usepackage[titles,subfigure]{tocloft} % Alter the style of the Table of Contents
\renewcommand{\cftsecfont}{\rmfamily\mdseries\upshape}
\renewcommand{\cftsecpagefont}{\rmfamily\mdseries\upshape} % No bold!

\newcommand{\N}{\mathbb{N}}
\newcommand{\Prob}{\mathbb{P}}
\newcommand{\E}{\mathbb{E}}
\newcommand{\Var}{\text{Var}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\Q}{\mathbb{Q}}
%%% END Article customizations

%%% The "real" document content comes below...

\date{} % Activate to display a given date or no date (if empty),
         % otherwise the current date is printed 

\begin{document}
\config{name}{Distributions and Continuous Probability}
\noindent{\bf Distributions and Continuous Probability.}

\begin{enumerate}
%------------------------------------------------------------------------------------------------------------------------
\item {\bf Which Distribution?} For each of the random variables below, choose the corresponding distribution. 
\begin{enumerate}
\item The random variable $X_n$ corresponding to the total number of heads resulting from $n$ coinflips of a coin which gives heads with probability $p$. 
\begin{Choices}
Review the definitions of each of the distributions below.
\begin{enumerate}
\TrueChoice\item Binomial
\FalseChoice\item Poisson
\FalseChoice\item Geometric
\FalseChoice\item Uniform
\Solution This is a classic example of the binomial distribution. The binomial distribution counts the number of successes in $n$ independent trials if each trial has a probability $p$ of success. Here success is getting a heads, $n$ is the number of coin flips, and $p$ is the probability of heads.
\end{enumerate}
\end{Choices}
%------------------------------
\item The random variable $Y$ corresponding to the number of times you need to roll two standard 6-sided dice before you get snake eyes (two ones). 
\begin{Choices}
Review the definitions of each of the distributions below.
\begin{enumerate}
\FalseChoice\item Binomial
\FalseChoice\item Poisson
\TrueChoice\item Geometric
\FalseChoice\item Uniform
\Solution This is a geometric random variable. The geometric distribution counts the number of independent trials until the first success, if each trial has a probability $p$ of success. Here success is rolling a snake eyes and $p$ is the probability of snake eyes.
\end{enumerate}
\end{Choices}
%------------------------------
\item The random variable $W$ corresponding to the number of minutes you have to wait for the bus, assuming the bus drives by your house every hour and you begin waiting at a random time. 
\begin{Choices}
Review the definitions of each of the distributions below.
\begin{enumerate}
\FalseChoice\item Binomial
\FalseChoice\item Poisson
\FalseChoice\item Geometric
\TrueChoice\item Uniform
\Solution This is a uniform distribution. How long you have to wait for the bus is determined entirely by how long it is until the next hour. Because you arrive at a random time, you could wait anywhere from $0$ minutes to $1$ hour with uniform likelihood. The two parameters here are $0$ and $1$, in units of hours.
\end{enumerate}
\end{Choices}
%------------------------------
\item The random variable $R$ corresponding to the number of pandagrader regrade requests in one week, assuming that each student independently decides whether to submit a request, and that there are on average 10 requests per week.
\begin{Choices}
Review the definitions of each of the distributions below.
\begin{enumerate}
\FalseChoice\item Binomial
\TrueChoice\item Poisson
\FalseChoice\item Geometric
\FalseChoice\item Uniform
\Solution This is a Poisson distribution. A Poisson random variable counts the number of independent arrivals in a unit interval given an average rate of arrival $\lambda$. Here an arrival is a regrade request, and the parameter $\lambda$ is $10 \text{ requests} / \text{week}$.
\end{enumerate}
\end{Choices}
%------------------------------
\item The random variable $T$ corresponding to the number of random guesses you make on this particular multiple choice problem before you get it right, assuming that each time you forget which choice you selected previously. 
\begin{Choices}
Review the definitions of each of the distributions below.
\begin{enumerate}
\FalseChoice\item Binomial
\FalseChoice\item Poisson
\TrueChoice\item Geometric
\FalseChoice\item Uniform
\Solution This is a geometric distribution. Recall that a geometric random variable counts the number of independent trials until a success, if each trial has a probability $p$ of succeeding. Because you forget the answers you selected previously, your probability of correctly guessing remains the same, so your situation fits this model.
\end{enumerate}
\end{Choices}
%-----------------------------------
\item The random variable $C$ corresponding to the total number of correct answers to $n$ true or false answers, if you guess true or false with equal probability for every question. 
\begin{Choices}
Review the definitions of each of the distributions below.
\begin{enumerate}
\TrueChoice\item Binomial
\FalseChoice\item Poisson
\FalseChoice\item Geometric
\FalseChoice\item Uniform
\Solution This is a binomial distribution. Recall that a binomial distribution counts the number of successes in $n$ independent trials, if each trial has a probability $p$ of success. Here success is answering the true/false question correctly, and because you guess true or false with equal probability for each question, the trials are independent and identically distributed, as the binomial distribution requires.
\end{enumerate}
\end{Choices}
%-----------------------------------
\end{enumerate}
%------------------------------------------------------------------------------------------------------------------------
\item {\bf Expectation of Random Variables by Distribution.} For each of the following random variables, you are given information about their distributions. Compute their means.
\begin{enumerate}
%-----------------------------------
\item Suppose that $X$ is a binomial random variable corresponding to the number of successes in $100$ trials with success probability $p = 0.45$. What is $\E[X]$?
\begin{Freeform}{45}
$\E[X]$ = 
\Hint Either derive from first principles, or review the course notes on the binomial distribution.
\Solution Recall that $\E[Binom(n, p)]\ =\ np$. Here $n = 100$ and $p = 0.45$, so $\E[X]\ =\ 45$.
\end{Freeform}
%-----------------------------------
\item Suppose that $Y$ is a Poisson random variable corresponding to the number of text messages you receive per hour, with rate $\lambda = 2$. What is $\E[Y]$?
\begin{Freeform}{2}
$\E[Y]$ = 
\Hint Either derive from first principles, or review the course notes on the poisson distribution.
\Solution Recall that $\E[Poiss(\lambda)]\ =\ \lambda$, so $\E[Y]\ =\ 2$.
\end{Freeform}
%-----------------------------------
\item Suppose that $Z$ is a geometric random variable with parameter $p = 0.2$. What is $\E[Z]$?
\begin{Freeform}{5}
$\E[Z]$ = 
\Hint Either derive from first principles, or review the course notes on the geometric distribution.
\Solution Recall that $\E[Geom(p)]\ =\ \frac{1}{p}$, so $\E[Z]\ =\ \frac{1}{0.2}\ =\ 5$
\end{Freeform}
What is $\Pr[Z \ge 5]$? Enter your answer as a decimal with leading 0.
\begin{Freeform}{0.4096}
$\Pr[Z \ge 5]$ = 
\Hint Review the course notes on the geometric distribution.
\Solution Recall that the probability mass function of a geometric random variable $X \sim Geom(p)$ is 

$$P(X = n)\ =\ p(1 - p)^{n - 1} \forall n \in \mathbb{Z^+}$$

Here $Z \sim Geom(0.2)$. We can express $P(Z \ge 5)$ as 

$$1 - P(Z < 5)\ =\ 1\ -\ \sum_{i = 1}^{4}\, 0.2 \times (0.8)^{i - 1}\ \approx\ 0.4096$$
\end{Freeform}
\end{enumerate}

%------------------------------------------------------------------------------------------------------------------------

\item {\bf Continuous Probability Spaces.} This problem will give you some practice with variances. 
\begin{enumerate}
%-----------------------------------
\item Let $X$ be a random real number drawn from the interval $[0,1]$. What is $\Pr[X = \frac{1}{2}]$?
\begin{Freeform}{0}
$\Pr[X = \frac{1}{2}] =$
\Hint Review the notes on continuous uniform probability spaces.
\Solution There are an infinite number of possible values $X$ could take on, so the probability that it's any one specific value is, loosely speaking, $\frac{1}{\infty}\ =\ 0$.
\end{Freeform}
What is $\E[X]$?
\begin{Freeform}{0.5}
$\E[X]=$
\Hint Review the notes on continuous uniform probability spaces.
\Solution In the continuous realm, the expectation of a random variable $X$ is given by 

$$\int_{-\infty}^{\infty}\, x\, f_X(x)\, dx$$

where $f_X(x)$ is the {\it probability density function}. While the height of $f_X(x)$ is meaningless, the area under the curve in any interval $[a, b]$ represents the probability that $X$ is inside $[a, b]$.\\

In this case, $X$ is uniform between $0$ and $1$. Its probability density function is 

$$f_{X}(x)\ =\ \begin{cases}1 & 0 \leq x \leq 1\\ 0 & \text{ else} \end{cases}$$

Thus the expectation is 

$$\E[X]\ =\ \int_{-\infty}^{\infty}\, x\, f_X(x)\, dx\ =\ \int_{0}^{1}\, x\, dx\ =\ 0.5$$

This is intuitively apparent: if it's uniformly likely to fall anywhere between $0$ and $1$, we would expect the average value to be at the halfway point. In general, expectations correspond to the ``center of mass'' of the probability density function.
\end{Freeform}
%-----------------------------------
\item Let $X$ be a continuous random variable with probability density function $\Pr[a \le X \le b] =M\int_{a}^{b} x^2 dx$ for all $-1 \le a \le b \le 1$, and let the probability be zero outside of the interval $[-1,1]$.
\begin{enumerate}
\item Given that the total probability of the space has to be 1, we know that 
\[
1 = \Pr[-1 \le X \le 1] = M\int_{-1}^{1} x^2 dx.
\]
What is the value of $M$? Enter your answer as a fraction of the form $x/y$ where $x, y$ are the smallest possible non-negative integers.
\begin{Freeform}{3/2}
$M =$
\Hint Review the notes on continuous uniform probability spaces and probability density functions.
\Solution Because $\int_{-1}^{1}\, x^2\, dx\ =\ \frac{2}{3}$, we need $M = \frac{3}{2}$ to make the total integral $1$.
\end{Freeform}
%%---------------
\item What is the probability that $X$ is positive? Enter your answer as a fraction of the form $x/y$ where $x, y$ are the smallest possible non-negative integers.
\begin{Freeform}{1/2}
$\Pr[X>0] =$
\Hint Review the notes on continuous uniform probability spaces and probability density functions.
\Solution Here the density function $f_X(x)$ is 

$$f_X(x)\ =\ \begin{cases}x^2 & -1 \leq x \leq 1 \\ 0 & \text{ else}\end{cases}$$ 

The probability that $x$ is positive is given by  

$$\Pr[X \ge 0]\ =\ \int_{0}^{\infty}\, f_X(x)\, dx\ =\ \int_{0}^{1}\, \left(\frac{3}{2} x^2\right)\, dx\ =\ \frac{1}{2}$$

We could also have arrived at this answer by inspection: the probability density function is symmetric around $0$, so half the mass is over positive $x$ and half over negative.
\end{Freeform}
%%---------------
\item What is the probability that $|X|$ is greater than $\frac{1}{2}$? Enter your answer as a fraction of the form $x/y$ where $x, y$ are the smallest possible non-negative integers.
\begin{Freeform}{7/8}
$\Pr[|X| \ge \frac{1}{2}] =$
\Hint Review the notes on continuous uniform probability spaces and probability density functions.
\Solution There are two ways $|X|$ could be greater than $\frac{1}{2}$: $X > \frac{1}{2}$ or $X < -\frac{1}{2}$. The probability of this is given by 

$$\int_{-\infty}^{-\frac{1}{2}}\, f_X(x)\, dx\ +\ \int_{\frac{1}{2}}^{\infty}\, f_X(x)\, dx$$

Substituting in $f_X(x)$,

 $$\int_{-1}^{-\frac{1}{2}}\, \left(\frac{3}{2}\, x^2\right)\, dx\ +\ \int_{\frac{1}{2}}^{1}\, \left(\frac{3}{2}\, x^2\right)\, dx\ =\ \frac{7}{8}$$ 
\end{Freeform}
%%---------------
\item What is the expected value of $X$?
\begin{Freeform}{0}
$\E[X] =$
\Hint Review the notes on expectation in continuous uniform probability spaces and probability density functions.
\Solution The expected value is given by 

$$\E[X]\ =\ \int_{-\infty}^{\infty}\, x\, f_X(x)\, dx\ =\ \int_{-1}^{1}\, x\, \left(\frac{3}{2}\, x^2\right)\, dx\ =\ 0$$

We could also have noticed by inspection that the center of mass of the probability density function was $0$, becuase it was symmetric about $0$. 

\end{Freeform}
%%---------------
\item What is the variance of $X$? Enter your answer as a fraction of the form $x/y$ where $x, y$ are the smallest possible non-negative integers.
\begin{Freeform}{3/5}
$\Var[X]=$
\Hint Review the notes on variance in continuous uniform probability spaces and probability density functions.
\Solution The definition of variance remains the same in continuous time:

$$\Var[X]\ =\ \E[X^2]\ -\ \E[X]^2$$

We know $\E[X]^2\ =\ 0$, so $\Var[X]\ =\ \E[X^2]$. We can calculate this by plugging $x^2$ into the formula for expectation:

$$\Var[X]\ =\ \E[X^2]\ =\ \int_{-\infty}^{\infty}\, x^2\, f_X(x)\, dx\ =\ \int_{-1}^{1}\, x^2\, \left(\frac{3}{2}\, x^2\right)\, dx\ =\ \frac{3}{5}$$
\end{Freeform}
%%---------------------------------------------
\end{enumerate}
\item Let $X$ be a different continuous random variable with probability density function $\Pr[a \le X \le b] = M\int_{a}^{b} x^{3} dx$ for all $0 \le a \le b \le 1$, and let the probability be zero outside of the interval $[0,1]$.
\begin{enumerate}
\item What is the value of $M$?
\begin{Freeform}{4}
$M =$
\Hint Review the notes on continuous uniform probability spaces and probability density functions.
\Solution We know that the density function must integrate to $1$:

$$M\, \int_{0}^{1}\, x^3\, dx\ =\ \frac{M}{4}\, =\, 1$$

Therefore, $M = 4$.
\end{Freeform}
%%---------------
\item What is the probability that $X$ is greater than $\frac{1}{2}$? Enter your answer as a fraction of the form $x/y$ where $x, y$ are the smallest possible non-negative integers.
\begin{Freeform}{15/16}
$\Pr[X > \frac{1}{2}] =$
\Hint Review the notes on continuous uniform probability spaces and probability density functions.
\Solution The probability that $X > \frac{1}{2}$ is given by 

$$\int_{\frac{1}{2}}^{\infty}\, f_X(x)\, dx\ =\ \int_{\frac{1}{2}}^{1}\, 4 x^3\, dx\ =\ \frac{15}{16}$$
\end{Freeform}
%%---------------
\item What is the expected value of $X$? Enter your answer as a fraction of the form $x/y$ where $x, y$ are the smallest possible non-negative integers.
\begin{Freeform}{4/5}
$\E[X] =$
\Hint Review the notes on expectation in continuous uniform probability spaces and probability density functions.
\Solution The expectation is given by

$$\E[X]\ =\ \int_{-\infty}^{\infty}\, x\, f_X(x)\, dx\ =\ \int_{0}^{1}\, x\, \left(4\, x^3\right)\, dx\ =\ \frac{4}{5}$$

This should make intuitive sense --- because $4\, x^3$ is rising through the interval $0, 1$, the center of mass should be weighted to the right side. 
\end{Freeform}
%%---------------
\item What is the variance of $X$? Enter your answer as a fraction of the form $x/y$ where $x, y$ are the smallest possible non-negative integers.
\begin{Freeform}{2/75}
$\Var[X]=$
\Hint Review the notes on variance in continuous uniform probability spaces and probability density functions.
\Solution Plugging in the defintion $\Var[X]\ =\ \E[X^2]\ -\ \E[X]^2$, we have 

$$\Var[X]\ =\ \int_{-\infty}^{\infty}\, x^2\, f_X(x)\, dx\ -\ \left(\frac{4}{5}\right)^2$$

Plugging in the density function,

$\Var[X]\ =\ \int_{0}^{1}\, x^2\, (4\, x^3)\, dx\ -\ \left(\frac{4}{5}\right)^2\ =\ \frac{2}{3} - \frac{16}{25}\ =\ \frac{2}{75}$
\end{Freeform}
%%---------------
\end{enumerate}
%%-----------------------------------
\end{enumerate}
%--------------------------------------------------------------------------------------------------------------------------

%--------------------------------------------------------------------------------------------------------------------------
\end{enumerate}
\end{document}
