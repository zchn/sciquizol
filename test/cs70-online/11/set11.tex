% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode

% This is a simple template for a LaTeX document using the "article" class.
% See "book", "report", "letter" for other types of document.

\documentclass[11pt,preview]{standalone} % use larger type; default would be 10pt

\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)


\usepackage{../../markup}
%%% Examples of Article customizations
% These packages are optional, depending whether you want the features they provide.
% See the LaTeX Companion or other references for full information.

%%% PAGE DIMENSIONS
\usepackage{geometry} % to change the page dimensions
\geometry{a4paper} % or letterpaper (US) or a5paper or....
% \geometry{margin=2in} % for example, change the margins to 2 inches all round
% \geometry{landscape} % set up the page for landscape
%   read geometry.pdf for detailed page layout information

\usepackage{graphicx} % support the \includegraphics command and options
\usepackage{color, tikz}
% \usepackage[parfill]{parskip} % Activate to begin paragraphs with an empty line rather than an indent

%%% PACKAGES
\usepackage{amsmath, amsfonts,amssymb}
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{paralist} % very flexible & customisable lists (eg. enumerate/itemize, etc.)
\usepackage{verbatim} % adds environment for commenting out blocks of text & for better verbatim
\usepackage{subfig} % make it possible to include more than one captioned figure/table in a single float
% These packages are all incorporated in the memoir class to one degree or another...

%%% HEADERS & FOOTERS
\usepackage{fancyhdr} % This should be set AFTER setting up the page geometry
\pagestyle{fancy} % options: empty , plain , fancy
\renewcommand{\headrulewidth}{0pt} % customise the layout...
\lhead{}\chead{}\rhead{}
\lfoot{}\cfoot{\thepage}\rfoot{}

%%% SECTION TITLE APPEARANCE
\usepackage{sectsty}
\allsectionsfont{\sffamily\mdseries\upshape} % (See the fntguide.pdf for font help)
% (This matches ConTeXt defaults)

%%% ToC (table of contents) APPEARANCE
\usepackage[nottoc,notlof,notlot]{tocbibind} % Put the bibliography in the ToC
\usepackage[titles,subfigure]{tocloft} % Alter the style of the Table of Contents
\renewcommand{\cftsecfont}{\rmfamily\mdseries\upshape}
\renewcommand{\cftsecpagefont}{\rmfamily\mdseries\upshape} % No bold!

\newcommand{\N}{\mathbb{N}}
\newcommand{\Prob}{\mathbb{P}}
\newcommand{\E}{\mathbb{E}}
\newcommand{\Var}{\text{Var}}
\newcommand{\Cov}{\text{Cov}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\Q}{\mathbb{Q}}
%%% END Article customizations

%%% The "real" document content comes below...

\date{} % Activate to display a given date or no date (if empty),
         % otherwise the current date is printed 

\begin{document}
\config{name}{Random Variables and Probability Distributions (continued)}
\noindent{\bf Random Variables, and Probability Distributions.}

\begin{enumerate}
%------------------------------------------------------------------------------------------------------------------------
\item {\bf Expectation and Variance.} This problem will give you some practice calculating expectations and variances of random variables.
\begin{enumerate}
\item Suppose that the random variable $X$ takes on 3 values, $10, 25, 70$. Suppose $\Prob[X =10] = 0.5$, $\Prob[X = 25] = 0.2$, and $\Prob[X = 70] = 0.3$. 
\begin{enumerate}
\item What is $\E[X]$?
\begin{Freeform}{31}
$\E[X]$ = 
\Hint Review the definition of expectation.
\Solution The expectation is 

$$\E[X]\ =\ 10 \times 0.5\ +\ 25 \times 0.2\ +\ 70 \times 0.3\ =\ 31$$
\end{Freeform}
\item What is $\E[X^2]$?
\begin{Freeform}{1645}
$\E[X^2]$ = 
\Hint Review the definition of expectation.
\Solution $X^2$ is $100$ with probability $0.5$, $625$ with probability $0.2$, and $4900$ with probability $0.3$, making the expectation 

$$\E[X^2]\ =\ 100 \times 0.5\ +\ 625 \times 0.2\ +\ 4900 \times 0.3\ =\ 1645$$
\end{Freeform}
\item What is $\Var[X]$?
\begin{Freeform}{684}
$\Var[X]$ = 
\Hint You can calculate this with the quantities above.
\Solution The variance is defined as $\E[X^2]\ -\ \E[X]^2$, giving

$$\Var[X]\ =\ 1645\ -\ 31^2\ =\ 684$$
\end{Freeform}
\end{enumerate}
%-----------------------------------
\item Let $X, Y$ be random variables, let $a,b,c$ be constants, and let $\E[X] = x$ and $\E[Y] = y$. What is $\E[aX + bY + c]$?
\begin{Choices}
Review linearity of expectation
\begin{enumerate}
\FalseChoice\item $a^2x + b^2y$
\FalseChoice\item $ax + by$
\TrueChoice\item $ax + by + c$
\FalseChoice\item $aX + bY + c$
\Solution The answer is $ax + by + c$. By linearity of expectation, we can split an expectation of sums into a sum of expectations, and we can further pull out constant factors.
\end{enumerate}
\end{Choices}
\item Let $X, Y$ be random variables, let $a,b,c$ be constants. Which of the following statements are always true?
\begin{Multi}
Review the definitions of expectation and variance, as well as properties of variance given in notes 12 and 13.
\begin{enumerate}
\TrueChoice\item $\E[aX + bY] = a\E[X]+ b\E[Y]$. 
\FalseChoice\item $\Var[aX + bY] = a\Var[X] + b\Var[Y]$
\TrueChoice\item $\Var[aX] = a^2\Var[X]$
\FalseChoice\item $\Var[aX + bY] = a^2\Var[X] + b^2\Var[Y]$
\TrueChoice\item $\Var[X + Y] = \Var[X] + \Var[Y]$ assuming $X,Y$ are independent.
\TrueChoice\item $\Var[X] + \E[X]^2 = \E[X^2]$
\TrueChoice\item $\Var[aX + c] = a^2\Var[X]$  (remember that $\Var[Y] = \E[(Y - \E[Y])^2]$)
\Solution Let's go through the propositions one by one.\\

\begin{enumerate}
\item $\E[aX + bY] = a\E[X]+ b\E[Y]$: True. This is the definition of linearity of expectation.\\

\item $\Var[aX + bY] = a\Var[X] + b\Var[Y]$: False. Variance is {\it not} in general linear. This only applies when $X$ and $Y$ are independent.\\

\item $\Var[aX] = a^2\Var[X]$: True. $\Var[aX]\, =\, \E[(aX)^2]\, -\, \E[aX]^2\, =\, a^2\left(\E[X^2]\, -\, \E[X]^2\right)\, =\, a^2 \Var[X]$. Note that we use linearity of expectation in the second step.\\

\item $\Var[aX + bY] = a^2\Var[X] + b^2\Var[Y]$: False. Expanding the variance expression, 
\begin{align*}
\Var[aX + bY]\ &=\ \E[(aX + bY)^2]\ -\ \E[aX + bY]^2\\
&=\ \E[a^2X^2\ +\ 2abXY\ +\ b^2Y^2]\ -\ \left(a^2\E[X]^2\ +\ 2 ab \E[X]\E[Y] + b^2 \E[Y]^2\right)\\
&= a^2\E[X^2]\ +\ 2 ab \E[XY] + b^2 \E[Y^2]\ -\ \left(a^2\E[X]^2\ +\ 2 ab \E[X]\E[Y] + b^2 \E[Y]^2\right)\\
&= a^2 \Var[X]\ +\ b^2 \Var[Y]\ -\ 2 ab (\E[XY]\ -\ \E[X]\E[Y])
\end{align*}

This statement is then only true when $\E[XY]\ -\ \E[X]\E[Y] = 0$. This quantity is called the covariance of $X$ and $Y$, denoted $\Cov[X, Y]$. When it's $0$, $X$ and $Y$ are called ``uncorrelated''.\\

\item $\Var[X + Y] = \Var[X] + \Var[Y]$ assuming $X,Y$ are independent: True. This quantity is the same as the previous quantity, with $a = b = 1$. Then the expression for $\Var[X + Y]$ is $\Var[X] + \Var[Y]\ -\ 2\,  \Cov[X, Y]$. Because independent random variables are necessarily uncorrelated, $\Cov[X, Y]\ =\ 0$, so this simply becomes $\Var[X]\ +\ \Var[Y]$.\\

\item $\Var[X] + \E[X]^2 = \E[X^2]$: True. This follows from the definition of $\Var[X]$ as $\E[X^2]\ -\ \E[X]^2$.\\

\item $\Var[aX + c] = a^2\Var[X]$: True. From the definition of variance,
\begin{align*}
\Var[aX + c]\ &=\ \E[(aX + c)^2]\ -\ \E[aX + c]^2\\
&=\ \E[a^2X^2\, +\, 2 a c X\, +\, c^2]\ -\ (a \E[X] + c)^2\\
&=\ a^2 \E[X^2]\, +\, 2 a c \E[X]\, +\, c^2\ -\ (a^2 \E[X]^2\, +\, 2 a c \E[X]\, +\, c^2)\\
&=\ a^2(\E[X^2]\, -\, \E[X]^2)\ +\ 0\\
&=\ a^2 \Var[X]
\end{align*}
\end{enumerate} 

\end{enumerate}
\end{Multi}
\end{enumerate}
%------------------------------------------------------------------------------------------------------------------------
\item {\bf A Game--Part I.} This game will give you practice with some expectations. 
\begin{enumerate}
%-----------------------------------
\item Suppose I have a bag full of equal numbers of \$1 bills and \$5 bills. If I let you choose a bill uniformly at random from this bag, let $X$ be the random variable corresponding to your profit. What is your expected profit?
\begin{Freeform}{3}
$\E[X]$ = 
\Hint Review the definition of expectation.
\Solution $X$ takes on the value \$1 with probability $0.5$ and \$5 with probability $0.5$. So our expected profit is $0.5 \times 1\, +\, 0.5 \times 5\ =\ 3$.
\end{Freeform}
%-----------------------------------
\item Say that I now add a second bag, which has equal numbers of $\$10$ and $\$20$ bills. Let the random variable corresponding to your profit from this bag be $Y$. If I let you choose one bill from the first bag and one bill from the second bag, what is your expected proft?
\begin{Freeform}{18}
$\E[X + Y]$ = 
\Hint Review the definition of expectation and linearity of expectation.
\Solution We know the expected profit from $X$ is \$3. The variable $Y$, representing profit from the second bag, is \$10 with probability $0.5$ and \$20 with probability $0.5$, meaning $\E[Y]\ =\ 10 \times 0.5\, +\, 20 \times 0.5\ =\ 15$. By linearity of expectation, $\E[X + Y]\ =\ \E[X]\, +\, \E[Y]\ =\ \$18$.
\end{Freeform}
%-----------------------------------
\item Now, suppose I decide to charge you $\$2.50$ to draw a bill from the first bag, and \$15.50 to draw from the second bag. If you draw from both bags, what is your expected net profit?
\begin{Freeform}{0}
$\E[X + Y - 18]$ = 
\Hint Review the definition of expectation and linearity of expectation.
\Solution The expected profit without charge was \$18, and you are now being charged \$18, so your expected profit is 0.
\end{Freeform}
%-----------------------------------
\item Given the pay scheme above (\$2.50 to draw from the first bag, \$15.50 to draw from the second bag) Which of the following actions gives the maximum expected profit?
\begin{Multi}
\begin{enumerate}
\TrueChoice \item Drawing from the first bag only.
\FalseChoice \item Drawing from the second bag only.
\FalseChoice \item Drawing from both bags.
\FalseChoice \item Drawing from neither bag.
\Solution Drawing from the first bag only would maximize expected profit. The expected money from the first bag is \$3, so at a fee of \$2.50, you make \$0.50 on average. We saw drawing from both bags leaves you with an expected profit of $0$, as does drawing from neither bag. Drawing from the second bag puts you at a loss --- you pay \$15.50 for an expected amount of \$15.00, making your expected profit -\$0.50.
\end{enumerate}
\end{Multi}
%-----------------------------------
\item As the designer of this game, I want your expected profit to be net negative. I have told you that the proportions of \$1 and \$5 bills in the bag are the same. However, I could lie to you and rig the game so that if I charge you \$2.50, your expected profit for drawing from the first bag is negative. What percentage of bills in the bag should be \$1 bils in order to make your expected profit $-\$1$ (round up to the nearest percent)?
\begin{Freeform}{88}
\% of \$1s? = 
\Hint Review the definition of expectation.
\Solution If the probability of a \$1 is $p$, then the expected profit from playing the game is 

$$1 \times p\ +\ 5 \times (1 - p)\ -\ 2.5\ =\ 2.5\ -\ 4 p$$

We want this expectation to be equal to $-1$:

$$2.5\ -\ 4 p\ =\ -1$$

Solving for $p$, we find $p = 0.875$. Rounded to the nearest percent, that's $88\%$.

\end{Freeform}
%-----------------------------------
\end{enumerate}

%------------------------------------------------------------------------------------------------------------------------

\item {\bf A Game--Part II.} This problem will give you some practice with variances. 
\begin{enumerate}
%-----------------------------------
\item Recall the game from Part I: I have a bag full of equal numbers of \$1 bills and \$5 bills. If I let you choose a bill uniformly at random from this bag (without charging you a fee), let $X$ be the random variable corresponding to your profit. What is the variance of $X$?
\begin{Freeform}{4}
$\Var[X] = \E[X^2] - \E[X]^2$ = 
\Hint Review the definition of variance.
\Solution We know from Part I that $\E[X]\, =\, 3$. To calculate the variance, we need $\E[X^2]$. $X^2$ is $1$ with probability $0.5$ and $25$ with probability $0.5$, giving $\E[X^2]\, =\, 1 \times 0.5\, +\, 25 \times 0.5\, =\, 13$. Then $\Var[X]\, =\, 13\, -\, 3^2\, =\, 4$
\end{Freeform}
%-----------------------------------
\item Say now that I have a new game, in which I do not make any claims about the composition of the bag. However, I do advertise that the {\bf average earnings for one round are \$3}, that {\bf the variance is 3}, and that each round is independent of the previous round. 

After playing the game $10$ times, your total earnings are $\$14$--you drew \$1 bills 9 out of 10 times. You suspect me of cheating customers at this game. Can you use expectation and variance to make a case against me? Intuitively the answer is yes--you can show that your earnings are consistently below the advertized expectation, in a way that is more extreme than the advertized variance predicts. 

This intuition can be formalized by \emph{Chebyshev's Inequality}. Chebyshev's Inequality states that for a random variable $X$, 
\[
\Pr[ |X - \E[X]| \ge a] \le \frac{\Var[X]}{a^2}.
\] 
\begin{enumerate}
%%-----------------------------------
\item Define $Y = \sum_{i=1}^{10}{\frac{1}{10}Y_i}$ to be the random variable corresponding to your average earnings, and let $Y_i$ be the random variable corresponding to your earnings the $i$th time you play. What is $\E[Y]$, if my claims about the expectation and variance are true? Round your answer to the nearest hundredth.
\begin{Freeform}{3.00}
$\E[Y]$ =  
\Hint Remember linearity of expectation. Also, this is not for the 10 games you already played--this is the expected earnings for 10 arbitrary games.
\Solution The expectation of each individual $Y_i$ is $\$3$, so by linearity their expected average will also be $\$3$. 
\end{Freeform}
%%-----------------------------------
\item What is $\Var[Y]$, if my claims are correct? Recall your answer from problem 1c, and that each time you play is independent of the others. Enter your answer as a decimal with leading 0.
\begin{Freeform}{0.3}
$\Var[Y]$ =  
\Hint Remember how variances of sums of independent random variables behave.
\Solution Recall that if $X$ and $Y$ are independent,

$$\Var[\alpha X + \beta Y]\ =\ \alpha^2 \Var[X]\ +\ \beta^2 \Var[Y]$$

Because each of these experiments is independent, 

$$\Var\left[\sum_{i = 1}^{10} \frac{1}{10} Y_i \right]\ =\ \frac{1}{100} \sum_{i = 1}^{10} \Var[Y_i]\ =\ \frac{1}{100} \times 10 \times 3\ =\ 0.3$$
\end{Freeform}
%%-----------------------------------
\item Let $|1.40- \E[Y]| = a$. Give an upper bound on the probability that $Y = \$1.40$  (if my claims about the expectation and variance are true), by using Chebyshev's Inequality to upper bound the probability that $|Y - \E[Y]| \ge a$. Give your answer as a decimal, rounding to the nearest hundredth.
\begin{Freeform}{0.12}
$\Pr[Y = \$1.40] \le \Pr[|Y - \E[Y]| \ge a] \le $  
\Hint Review the definition of expectation and linearity of expectation.
\Solution The probability that $Y = \$1.40$ is at least as much as the probability that $Y \leq \$1.40$: 

$$P(Y = \$1.40)\ \leq\ P(Y \leq \$1.40)$$

If $Y \leq \$1.40$, it is smaller than its supposed mean by $\$1.60$:

$$P(Y = \$1.40)\ \leq\ P(Y\ -\ 3 \leq -\$1.60)$$

The probability that $Y$ deviates from its mean on the {\it left }is at least as high as the probability that it deviates on {\it both }sides:

$$P(Y = \$1.40)\ \leq\ P(\left| Y\ -\ 3 \right| \geq \$1.60)$$

We now have the probability in a form that can be used with Chebyshev's inequality: 

$$P(Y = \$1.40)\ \leq\ \frac{\Var[Y]}{(\$1.60)^2}\ =\ \frac{0.3}{(\$1.60)^2}\ \approx\ 0.12$$

\end{Freeform}
%%-----------------------------------
\item Say you decided that this probability is not convincing enough. You decided to repeat the game 100 times, and after these 100 times you find that your average earnings are still \$1.40.

Define a new random variable corresponding to your average earnings, $Z =\sum_{i=1}^{100} \frac{1}{100} Z_i$, where $Z_i$ is your earnings on the $i$th round. Repeat the process above (calculate $\Var[Z]$, then use Chebyshev's Inequality) to give the best upper bound you can on the probability that $Z = \$1.40$. Give your answer as a decimal, rounding to the nearest hundredth.
\begin{Freeform}{0.01}
$\Pr[Z = \$1.40] \le  \Pr[|Z - \E[Z]| \ge a] \le$  
\Hint Review the definition of expectation and linearity of expectation.
\Solution The expectation of $Z$ still remains \$3. The variance is now given by 

$$\Var[Z]\ =\ \Var\left[\sum_{i = 1}^{100} \frac{1}{100} Y_i \right]\ =\ \frac{1}{10000} \times 100 \times 3\ =\ 0.03$$.

Using the same logic as above, we can bound the probability that we got an average of \$1.40 like so:

$$P(Z\ =\ \$1.40)\ \leq\ \frac{\Var[Z]}{(\$1.60)^2}\ =\ \frac{0.03}{(\$1.60)^2}\ \approx\ 0.01$$

\end{Freeform}
%%-----------------------------------
\item Even though the average is the same after 10 and 100 games, do you have a stronger case for my cheating after repeating the experiment more times?
\begin{Choices}
\begin{itemize}
\TrueChoice \item Yes
\FalseChoice \item No
\Solution Yes, because the variance of the average of $100$ trials is smaller than that of $10$ trials.
\end{itemize}
\end{Choices}
\end{enumerate}

%\item Now, suppose I decide to charge you $\$2.50$ to draw a bill from the first bag, and \$15.50 to draw from the second bag. If you draw from both bags, what is your expected net profit?
%\begin{Freeform}{0}
%$\E[X + Y - 18]$ = 
%\Hint Review the definition of expectation and linearity of expectation.
%\end{Freeform}
%%-----------------------------------
%\item Given the pay scheme above (\$2.50 to draw from the first bag, \$15.50 to draw from the second bag) Which of the following actions maximizes your expected profit?
%\begin{Multi}
%\begin{enumerate}
%\TrueChoice \item Drawing from the first bag only.
%\FalseChoice \item Drawing from the second bag only.
%\FalseChoice \item Drawing from both bags.
%\FalseChoice \item Drawing from neither bag.
%\end{enumerate}
%\end{Multi}
%%-----------------------------------
%\item As the designer of this game, I want your expected profit to be net negative. I have told you that the proportions of \$1 and \$5 bills in the bag are the same. However, I could lie to you and rig the game so that if I charge you \$2.50, your expected profit for drawing from the first bag is negative. What percentage of bills in the bag should be \$1 bils in order to make your expected profit $-\$1$ (round up to the nearest percent)?
%\begin{Freeform}{88}
%\% of \$1s? = 
%\Hint Review the definition of expectation.
%\end{Freeform}
%%-----------------------------------
\end{enumerate}

%--------------------------------------------------------------------------------------------------------------------------

%--------------------------------------------------------------------------------------------------------------------------
\end{enumerate}
\end{document}
