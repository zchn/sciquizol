% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode

% This is a simple template for a LaTeX document using the "article" class.
% See "book", "report", "letter" for other types of document.

\documentclass[11pt,preview]{standalone} % use larger type; default would be 10pt

\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)


\usepackage{../../markup}
%%% Examples of Article customizations
% These packages are optional, depending whether you want the features they provide.
% See the LaTeX Companion or other references for full information.

%%% PAGE DIMENSIONS
\usepackage{geometry} % to change the page dimensions
\geometry{a4paper} % or letterpaper (US) or a5paper or....
% \geometry{margin=2in} % for example, change the margins to 2 inches all round
% \geometry{landscape} % set up the page for landscape
%   read geometry.pdf for detailed page layout information

\usepackage{graphicx} % support the \includegraphics command and options
\usepackage{color, tikz}
% \usepackage[parfill]{parskip} % Activate to begin paragraphs with an empty line rather than an indent

%%% PACKAGES
\usepackage{amsmath, amsfonts,amssymb}
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{paralist} % very flexible & customisable lists (eg. enumerate/itemize, etc.)
\usepackage{verbatim} % adds environment for commenting out blocks of text & for better verbatim
\usepackage{subfig} % make it possible to include more than one captioned figure/table in a single float
% These packages are all incorporated in the memoir class to one degree or another...

%%% HEADERS & FOOTERS
\usepackage{fancyhdr} % This should be set AFTER setting up the page geometry
\pagestyle{fancy} % options: empty , plain , fancy
\renewcommand{\headrulewidth}{0pt} % customise the layout...
\lhead{}\chead{}\rhead{}
\lfoot{}\cfoot{\thepage}\rfoot{}

%%% SECTION TITLE APPEARANCE
\usepackage{sectsty}
\allsectionsfont{\sffamily\mdseries\upshape} % (See the fntguide.pdf for font help)
% (This matches ConTeXt defaults)

%%% ToC (table of contents) APPEARANCE
\usepackage[nottoc,notlof,notlot]{tocbibind} % Put the bibliography in the ToC
\usepackage[titles,subfigure]{tocloft} % Alter the style of the Table of Contents
\renewcommand{\cftsecfont}{\rmfamily\mdseries\upshape}
\renewcommand{\cftsecpagefont}{\rmfamily\mdseries\upshape} % No bold!

\newcommand{\N}{\mathbb{N}}
\newcommand{\Prob}{\mathbb{P}}
\newcommand{\E}{\mathbb{E}}
\newcommand{\Var}{\text{Var}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\Q}{\mathbb{Q}}
%%% END Article customizations

%%% The "real" document content comes below...

\date{} % Activate to display a given date or no date (if empty),
         % otherwise the current date is printed 

\begin{document}
\config{name}{Continuous Probability, CLT and Chernoff}
\noindent{\bf Continuous Probability, CLT and Chernoff.}

\begin{enumerate}
%------------------------------------------------------------------------------------------------------------------------
\item {\bf Famous Continuous Distributions.} Here, you will familiarize yourself with the exponential distribution and the normal distribution. 
\begin{enumerate}
\item Let the random variable $X$ be distributed according to the exponential distribution with parameter $\lambda = 0.2$. 
%------------------------------
\begin{enumerate}
\item Which of the following is the probability density function of $X$?
\begin{Choices}
\begin{enumerate}
\FalseChoice\item $p(x) = \left\{ \begin{array}{ll} e^{\lambda x} & x \ge 0 \\ 0 & \text{ otherwise} \end{array}\right.$
\FalseChoice\item  $p(x) = \left\{ \begin{array}{ll} \lambda e^{\lambda x} & x \ge 0 \\ \lambda e^{- \lambda x} & \text{ otherwise} \end{array}\right.$
\TrueChoice\item  $p(x) = \left\{ \begin{array}{ll} \lambda e^{-\lambda x} & x \ge 0 \\ 0 & \text{ otherwise} \end{array}\right.$
\Solution The correct answer is $C$; this is a known distribution found in the lecture notes and in other references.
\end{enumerate}
\end{Choices}
%------------------------------
\item What is $\E[X]$?
\begin{Freeform}{5}
$\E[X]=$
\Hint Review course notes on the exponential distribution, or use the pdf of $X$ and the definition of expectation.
\Solution The expectation of an exponential random variable with parameter $\lambda$ is $\frac{1}{\lambda}$, so 

$$\E[X]\ =\ \frac{1}{0.2}\ =\ 5$$
\end{Freeform}
%------------------------------
\item What is $\Var(X)$?
\begin{Freeform}{25}
$\Var(X)=$
\Hint Review course notes on the exponential distribution, or use the pdf of $X$ and the definition of variance.
\Solution The variance of an exponential random variable is $\frac{1}{\lambda^2}$, so 

$$\Var[X]\ =\ \left(\frac{1}{0.2}\right)^2\ =\ 25$$
\end{Freeform}
%------------------------------
\item What is $\Pr[X \le 0]$?
\begin{Freeform}{0}
$\Pr[X \le 0]=$
\Hint Review course notes on the exponential distribution, and use the pdf of $X$.
\Solution The exponential random variable is strictly positive, so the probability is $0$.
\end{Freeform}
%------------------------------
\item What is $\Pr[X \ge 5]$? Express your answer as a decimal with leading zero, rounded to the nearest hundredth.
\begin{Freeform}{0.37}
$\Pr[X \ge 5]=$
\Hint Review course notes on the exponential distribution, and use the pdf of $X$.
\Solution $\Pr[X \ge 5]$ is given by 

$$0.2 \int_{5}^{\infty}\, e^{-0.2 x}\, dx\ \approx\ 0.37$$
\end{Freeform}
%------------------------------
\end{enumerate}
%------------------------------
\item For which ranges of the parameter $\lambda$ is the exponential distribution a valid probability distribution? Choose all that apply.
\begin{Multi}
Review the definition of a probability distribution; for which of these parameters does the exponential pdf integrate to 1?
\begin{itemize}
\FalseChoice\item $\lambda \in (-\infty, 0)$
\FalseChoice\item $\lambda = 0$
\TrueChoice\item $\lambda \in (0, 1]$
\TrueChoice\item $\lambda \in (1, \infty)$
\Solution The integral will converge iff the exponent is less than $0$. This is satisfied when $\lambda > 0$, so the last two choices are both true.
\end{itemize}
\end{Multi} 
%------------------------------
\item Let the random variable $X$ be distributed according to the normal distribution with $\mu = 1$ and $\sigma = \frac{1}{2}$. 
%------------------------------
\begin{enumerate}
\item Which of the following is the probability density function of $X$?
\begin{Choices}
\begin{enumerate}
\FalseChoice\item $p(x) = \left\{ \begin{array}{ll}\frac{1}{\sqrt{2\pi\sigma^2}} e^{-(x-\mu)^2/2\sigma^2} & x \ge 0 \\ 0 & \text{ otherwise} \end{array}\right.$
\FalseChoice\item  $p(x) =\frac{1}{2\pi\sigma^2} e^{-(x-\mu)^2/2\sigma^2}$
\TrueChoice\item  $p(x) =\frac{1}{\sqrt{2\pi\sigma^2}} e^{-(\mu - x)^2/2\sigma^2}$
\Solution The correct choice is $C$: the normal random variable can take on any value in $[-\infty, \infty]$, ruling out $A$, and $B$ is missing a square root in the coefficient.
\end{enumerate}
\end{Choices}
%------------------------------
\item What is $\E[X]$?
\begin{Freeform}{1}
$\E[X]=$
\Hint Review course notes on the normal distribution, or use the pdf of $X$ and the definition of expectation.
\Solution The expectation is $\mu = 1$.
\end{Freeform}
%------------------------------
\item What is $\Var(X)$? Enter your answer as a fraction (i.e. in the form $x/y$ where $x,y$ are the smallest possible positive integers). 
\begin{Freeform}{1/4}
$\Var(X)=$
\Hint Review course notes on the normal distribution, or use the pdf of $X$ and the definition of variance.
\Solution The standard deviation $\sigma = \frac{1}{2}$, and the variance is defined as $\sigma^2$, so $\Var[X]\ =\ \frac{1}{4}$
\end{Freeform}
%------------------------------
\item What is $\Pr[X \le 1]$? Enter your answer as a fraction (i.e. in the form $x/y$ where $x,y$ are the smallest possible positive integers). 
\begin{Freeform}{1/2}
$\Pr[X \le 1]=$
\Hint Review course notes on the normal distribution. Calculate directly using the pdf of $X$, or reason about this.
\Solution Since $1$ is the mean and the entire distribution is symmetric about the mean, half the mass is to the left of $1$. Thus $\Pr[X \le 1]\ =\ \frac{1}{2}$.
\end{Freeform}
%------------------------------
\item What is $\Pr[X \ge 0.5]$? You can calculate this using a computational tool (such as Wolfram Alpha) to integrate the pdf over the appropriate interval. Express your answer as a decimal with leading zero, rounded to the nearest hundredth.
\begin{Freeform}{0.84}
$\Pr[X \ge 0.5]=$
\Hint Review course notes on the normal distribution, and use the pdf of $X$.
\Solution The probability is given by

$$ \frac{2}{\sqrt{2 \pi}}\, \int_{0.5}^{\infty}\, e^{-2(x - 1)^2}\, dx\ \approx\ 0.84$$
\end{Freeform}
%------------------------------
\end{enumerate}
\item Consider the function $f(x) = e^{-(x - \mu)^2/2\sigma^2}$. What is $\int_{-\infty}^{\infty} e^{-(x - \mu)^2/2\sigma^2}dx$?
\begin{Choices}
Think of the normal distribution. What does a probability distribution integrate to?
\begin{enumerate}
\FalseChoice\item $ e^{-\mu^2}$
\TrueChoice\item $\sqrt{2\pi \sigma^2}$
\FalseChoice\item $\frac{1}{2\pi \sigma}$
\Solution This is the same as the integral of the normal distribution times $\sqrt{2 \pi \sigma^2}$. Because the normal distribution integrates to $1$, this must integrate to $\sqrt{2 \pi \sigma^2}$
\end{enumerate}
\end{Choices}
%------------------------------
\item Let $X_1$ be a random variable with normal distribution with $\mu_1 = 0$ and $\sigma_1 = 1$, and let $X_2$ be a random variable with normal distribution with  $\mu_2 = 0$ and $\sigma_2 = 2$. 
%------------------------------
\begin{enumerate}
\item Graph the two distributions using your software of choice (for example, Wolfram Alpha is free). Which random variable is more tightly concentrated about its average?
\begin{Choices}
\begin{enumerate}
\TrueChoice\item $X_1$
\FalseChoice\item  $X_2$
\Solution $X_1$ should be more tightly concentrated --- a smaller variance corresponds to a lesser spread.
\end{enumerate}
\end{Choices}
%------------------------------
\item What is $\Pr[-\frac{1}{2} \le X_1 \le \frac{1}{2}]$?  You can calculate this using a computational tool (such as Wolfram Alpha) to integrate the pdf over the appropriate interval. Express your answer as a decimal with leading zero, rounded to the nearest hundredth.
\begin{Freeform}{0.38}
$\Pr[-\frac{1}{2} \le X_1 \le \frac{1}{2}]=$
\Hint Use the pdf of $X_1$ to calculate this.
\Solution The probability is given by 

$$\frac{1}{\sqrt{2 \pi}}\, \int_{-0.5}^{0.5}\, e^{-\frac{x^2}{2}}\, dx\ \approx\ 0.38$$
\end{Freeform}
%------------------------------
\item What is $\Pr[-\frac{1}{2} \le X_2 \le \frac{1}{2}]$?  You can calculate this using a computational tool (such as Wolfram Alpha) to integrate the pdf over the appropriate interval. Express your answer as a decimal with leading zero, rounded to the nearest hundredth.  Does this result in combination with part ii above match your observation from part i?
\begin{Freeform}{0.20}
$\Pr[-\frac{1}{2} \le X_2 \le \frac{1}{2}]=$
\Hint Use the pdf of $X_2$ to calculate this.
\Solution The probability is given by 

$$\frac{1}{2\, \sqrt{2 \pi}}\, \int_{-0.5}^{0.5}\, e^{-\frac{x^2}{8}}\, dx\ \approx\ 0.20$$

As expected, less of the mass is concentrated in the same interval around $0$
\end{Freeform}
%------------------------------
\item What is $\Pr[-\sigma_1 \le X_1 \le \sigma_1]$?  You can calculate this using a computational tool (such as Wolfram Alpha) to integrate the pdf over the appropriate interval. Express your answer as a decimal with leading zero, rounded to the nearest hundredth.
\begin{Freeform}{0.68}
$\Pr[-\sigma_1 \le X_1 \le \sigma_1]=$
\Hint Use the pdf of $X_1$ to calculate this.
\Solution Because $\sigma_1 = 1$, we compute

$$\frac{1}{\sqrt{2 \pi}}\, \int_{-1}^{1}\, e^{-\frac{x^2}{2}}\, dx\ \approx\ 0.68$$
\end{Freeform}
%------------------------------
\item What is $\Pr[-\sigma_2 \le X_2 \le \sigma_2]$?  You can calculate this using a computational tool (such as Wolfram Alpha) to integrate the pdf over the appropriate interval. Express your answer as a decimal with leading zero, rounded to the nearest hundredth. Compare this to your answer above. What do you notice?
\begin{Freeform}{0.68}
$\Pr[-\sigma_2 \le X_2 \le \sigma_2]=$
\Hint Use the pdf of $X_2$ to calculate this.
\Solution Because $\sigma_2 = 2$, we calculate

$$\frac{1}{2\, \sqrt{2 \pi}}\, \int_{-2}^{2}\, e^{-\frac{x^2}{8}}\, dx\ \approx\ 0.68$$
\end{Freeform}
%------------------------------
\end{enumerate}
\end{enumerate}
%------------------------------------------------------------------------------------------------------------------------
\item {\bf The Central Limit Theorem.} Let us return to a previous online homework problem: recall the game in which I have a bag full of $\$1$ and $\$5$ bills, and each round you draw a single bill. I advertise that the average profit of a single round is $\$3$, and that the variance is also $3$. 
\begin{enumerate}
\item Suppose you play $10$ rounds and have an average profit $A_{10}$ such that $A_{10} \le \$1.40$. Using the central limit theorem, what is the probability of this outcome for these 10 games? Use a tool (such as Wolfram Alpha, which is free) to integrate the pdf of the normal distribution over the appropriate interval. Round your answer to 5 decimal places (of the form $0.xxxxx$, where each $x$ is a digit).  
\begin{Freeform}{0.00174}
$\Pr[A_{10} \le \$1.40]=$
\Hint Use the central limit theorem. What is the distribution of the mean of the profits of these games?
\Solution From last time we know the claimed mean profit is $3$, and the claimed variance is $0.3$. We can approximate $A_{10}$ as $\hat{A}_{10} \sim Normal(3, 0.3)$. Then

$$\Pr[A_{10} \le \$1.40]\ \approx\ \Pr[\hat{A}_{10} \le \$1.40]$$

This is equal to 

$$\frac{1}{\sqrt{2 \pi (0.3)}} \int_{-\infty}^{1.4}\, e^{-\frac{(x - 3)^2}{2 \times 0.3}}\, dx\ \approx\ 0.00174$$ 
\end{Freeform}

\item Now, suppose you play $100$ rounds and have an average profit $A_{100}$ such that $A_{100} \le \$1.40$. Using the central limit theorem, what is the order of magnitude of the probability of this outcome for these 100 games? Use a tool (such as Wolfram Alpha, which is free) to integrate the pdf of the normal distribution over the appropriate interval, and then give the power of 10 that is the first nonzero digit. 
\begin{Freeform}{-20}
$\Pr[A_{100} \le \$1.40] \approx 1.3 \cdot 10^{x}$, \qquad  $x = $
\Hint Use the central limit theorem. What is the distribution of the mean of the profits of these games?
\Solution The situation is the same as above, except now the variance is $0.03$:

$$\frac{1}{\sqrt{2 \pi (0.03)}} \int_{-\infty}^{1.4}\, e^{-\frac{(x - 3)^2}{2 \times 0.03}}\, dx\ \approx\ 10^{-20}$$

So the first nonzero digit is digit -20.
\end{Freeform}
\end{enumerate}
%------------------------------------------------------------------------------------------------------------------------
%\item {\bf Conditional Independence.} 
%\begin{enumerate}
%%------------------------------
%\item Suppose you play $10$ rounds and have an average profit $A_{10}$ such that $A_{10} \le \$1.40$. Using the central limit theorem, what is the probability of this outcome for these 10 games? Round your answer to 5 decimal places (of the form $0.xxxxx$, where each $x$ is a digit).  
%\begin{Freeform}{0.00174}
%$\Pr[A_{10} \le \$1.40]=$
%\Hint Use the central limit theorem. What is the distribution of the mean of the profits of these games?
%\end{Freeform}
%%------------------------------
%\item Now, suppose you play $100$ rounds and have an average profit $A_{100}$ such that $A_{100} \le \$1.40$. Using the central limit theorem, what is the order of magnitude of the probability of this outcome for these 100 games? That is, give the power of 10 that is the first nonzero digit. 
%\begin{Freeform}{-20}
%$\Pr[A_{100} \le \$1.40] \approx 1.3 \cdot 10^{x}$, \qquad  $x = $
%\Hint Use the central limit theorem. What is the distribution of the mean of the profits of these games?
%\end{Freeform}
%%------------------------------
%\end{enumerate}
%--------------------------------------------------------------------------------------------------------------------------
\item \textbf{Chebyshev and Chernoff Bounds} Consider a biased
  coin with probability $p = 1/3$ of landing heads and probability
  $2/3$ of landing tails. Suppose the coin is flipped some number $n$
  of times, and let $X_i$ be a random variable denoting the $i^{th}$
  flip, where $X_i = 1$ means heads, and $X_i = 0$ means tails. Let
  random variable $X = \frac{1}{n}\sum_{i=1}^{n}X_i$. Compute the
  following expectation and varance:
  \begin{enumerate}
  \item What is $\E[X_i]$?
    \begin{Freeform}{1/3}
      $\E[X_i] = $
      \Solution The expectation of an individual $X_i$ is given by

      $$\E[X_i]\ =\ \frac{1}{3} \times 1\ +\ \frac{2}{3} \times 0\ =\ \frac{1}{3}$$    

    \end{Freeform}
  \item What is $\Var[X_i]$?
    \begin{Freeform}{2/9}
      $\Var[X_i] = $
      \Solution The variance of an individual $X_i$ is 
      $$\Var[X_i]\ =\ \E[X_i^2]\ -\ \E[X_i]^2\ =\ \frac{1}{3}\ -\ \frac{1}{9}\ =\ \frac{2}{9}$$
    \end{Freeform}
  \item What is $\E[X]$?
    \begin{Freeform}{1/3}
      $\E[X] = $
      \Solution By linearity, the expectation of $X$, the average of $n$ $X_i$, is the same as the expectation of an individual $X_i$, $\frac{1}{3}$.
    \end{Freeform}
  \item What is $\Var[X]$?
    \begin{Choices}
      \begin{enumerate}
      \FalseChoice\item $\frac{2}{9}$
      \TrueChoice\item $\frac{2}{9n}$
      \FalseChoice\item $\frac{1}{3}$
      \FalseChoice\item $\frac{1}{3n}$
      \Solution Because the $X_i$ are independent, the variance of their sum is the sum of their variances. The variance of the sum of $S_n$, the sum of $n$ $X_i$, is 

      $$\Var[S_n]\ =\ \Var\left[\sum_{i = 1}^{n}\, X_i\right]\ =\ n\, \Var[X_i]\ =\ \frac{2\, n}{9}$$ 

      However, we want the variance of the sum multiplied by $\frac{1}{n}$. From the scaling property of variance ($\Var[\alpha X]\ =\ \alpha^2\, \Var[X]$), the variance of the average is 

      $$\frac{1}{n^2}\, \Var[S_n]\ =\ \frac{1}{n^2}\, \frac{2\, n}{9}\ =\ \frac{2}{9\, n}$$
      \end{enumerate}
    \end{Choices}
    Now we try to use both the Chebyshev's Inequality and the Chernoff
    Bound to determine a value for $n$ so that the probability that more
    than half of the coin flips come out heads is less than 0.001.
  \item The Chebyshev's Inequality says that for a random variable $X$
    with expectation $\E[X] = \mu$, and for any $\alpha > 0$
    \begin{align}
      \Pr[|X-\mu|\ge\alpha]\le\frac{\Var[X]}{\alpha^2}
    \end{align}
    According to the definition of probability, we also have
    \begin{align}
      \Pr[X-\mu\ge\alpha]\le\Pr[|X-\mu|\ge\alpha]
    \end{align}
    To determine $n$, what should $\alpha$ be?
    \begin{Freeform}{1/6}
      $\alpha = $
      \Solution The mean is $\frac{1}{3}$, so if more than half of the coin flips are heads, then we deviated from the mean by 

      $$\alpha\ =\ \frac{1}{2}\ -\ \frac{1}{3}\ =\ \frac{1}{6}$$
    \end{Freeform}
  \item What is the minimum value of $n$ according to the Chebyshev Inequality?
    \begin{Freeform}{8000}
      $n = $
      \Solution According to the Chebyshev Inequality,

      $$P\left(X \geq \frac{1}{2}\right)\ <\ P\left(\left|X - \frac{2}{3} \right|\, \geq \frac{1}{6}\right)\ \leq\ \frac{\Var[X]}{\frac{1}{6^2}}\ =\ \frac{8}{n}$$

      If we want the probability to be no larger than $0.001$, 

      $$\frac{8}{n}\ \leq\ 0.001\ \implies\ n\ \geq\ 8000$$

      Thus the minimum value for $n$ according to Chebyshev's Inequality is $8000$.
    \end{Freeform}
  \item The Chernoff Inequality says if $X_i$s are i.i.d. and $X =
    \frac{1}{n}\sum_{i=1}{n}X_i$, then
    \begin{align}
      \Pr[X\ge\alpha]\le e^{-n\Phi_{X_1}(\alpha)}, \text{for} \alpha \ge p
      \text{or}
      \Pr[X\le\alpha]\le e^{-n\Phi_{X_1}(\alpha)}, \text{for} \alpha \le p
    \end{align}
    where $\Phi_{X_1}(\alpha)$ is called the Kullback-Liebler Divergence, usually denoted by $D(a||p)$
    \begin{align}
      D(a||p) = \Phi_{X_1}(\alpha) = \alpha \text{ln}\frac{\alpha}{p} + (1-\alpha)\text{ln}\frac{1-\alpha}{1-p}
    \end{align}
    To determine $n$, what should $\alpha$ be?
    \begin{Freeform}{1/2}
      $\alpha = $
      \Solution The Chernoff bound does not require a deviation from the mean, so $\alpha$ is simply $\frac{1}{2}$
    \end{Freeform}
  \item What is the minimum value of $n$ according to the Chernoff Bound?
    \begin{Freeform}{118}
      $n = $
      \Solution First we determine the value of $\Phi_{X_1}(\alpha)$, by plugging in $\alpha\, =\, \frac{1}{2}$ and $p\, =\, \frac{1}{3}$:

      $$\Phi_{X_1}\ =\ \frac{1}{2}\, \ln \frac{\frac{1}{2}}{\frac{1}{3}}\ +\ \left(1 - \frac{1}{2}\right)\, \ln \frac{1 - \frac{1}{2}}{1 - \frac{1}{3}}\ \approx\ 0.0589$$

      Given $\Phi_{X_1}$, we know 

      $$e^{-n\, \Phi_{X_1}(\alpha)}\ \leq\ 0.001$$

      Taking the natural log of both sides,

      $$-n\, \Phi_{X_1}(\alpha)\ \leq\ \ln 0.001$$

      Rearranging to solve for $n$,

      $$n\ \geq\ \frac{\ln 0.001}{-\Phi_{X_1}(\alpha)}$$

      Plugging in the value found above,

      $$n\ \geq\ \frac{\ln 0.001}{-0.0589}\ \approx\ 117.3$$

      Because we want to be conservative, we round up to get $118$.
    \end{Freeform}
  \end{enumerate}
% \item {\bf Cardinality.}
% For each of the following pairs of sets, decide whether they have the same cardinality, and if so select the appropriate bijection. 
% \begin{enumerate}
% %------------------------------
% \item The set $\N$ and the set $\Z$.
% \begin{Choices}
% Review course note 18 and course note 19.
% \begin{enumerate}
% \FalseChoice \item They have different cardinality.
% \FalseChoice\item  $f:\Z \to \N, f(x) = \left\{\begin{array}{ll} x & x \ge 0 \\ -x & x < 0 \end{array}\right.$
% \TrueChoice\item $f:\Z \to \N, f(x) = \left\{\begin{array}{ll} 2x - 1 & x > 0 \\ -2x & x \le 0 \end{array}\right.$
% \end{enumerate}
% \end{Choices}
% %------------------------------
% \item The set $\R$ and the set $\Z$.
% \begin{Choices}
% Review course note 18 and course note 19.
% \begin{enumerate}
% \TrueChoice \item They have different cardinality.
% \FalseChoice\item  $f:\R \to \Z, f(x) = \lceil x\rceil $
% \FalseChoice\item $f:\R \to \Z, f(x) = \lfloor x \rfloor$
% \end{enumerate}
% \end{Choices}
% %------------------------------
% \item The set of all perfect integer squares $S$ (excluding 0), and the set $\Z$.
% \begin{Choices}
% Review course note 18 and course note 19.
% \begin{enumerate}
% \FalseChoice \item They have different cardinality.
% \FalseChoice\item  $f:\Z \to S, f(x) =x^2$. 
% \TrueChoice\item $f:\Z \to S, f(x) = \left\{\begin{array}{ll} (2x)^2 & x > 0 \\ (2x + 1)^2 & x \le 0 \end{array}\right.$.
% \end{enumerate}
% \end{Choices}
% %------------------------------

% \item The set $\Z$ and the power set of $\Z$, $\mathcal{P}(\Z)$ (the set of all subsets of $\Z$).
% \begin{Choices}
% Review course note 18 and course note 19.
% \begin{enumerate}
% \TrueChoice \item They have different cardinality.
% \FalseChoice\item  $f:\mathcal{P}(\Z) \to \Z, f(S) = |S|$ (the size of the subset $S$).
% \FalseChoice\item $f:\mathcal{P}(\Z) \to \Z, f(S) = S|_1$ (the first element in the subset $S$). 
% \end{enumerate}
% \end{Choices}

% %------------------------------
% \item The set $\N\times \N$ (the set of all subsets of size 2 of $\N$), and the set $\N$.
% \begin{Choices}
% Review course note 18 and course note 19.
% \begin{enumerate}
% \FalseChoice \item They have different cardinality.
% \TrueChoice\item  $f:(\N\times\N) \to \N, f((x,y)) = (x+1)\cdot(y+1) - 1 + \sum_{i = 1}^{y} i + \sum_{j = 1}^{x-1} j$\\ (what order does this define for walking in the infinite grid?)
% \FalseChoice\item  $f:(\N\times\N) \to \N, f((x,y)) = x\cdot|\N| +  y$\\ (what order does this define for walking in the infinite grid?)
% \end{enumerate}
% \end{Choices}
% %------------------------------
% \end{enumerate}
%--------------------------------------------------------------------------------------------------------------------------
\end{enumerate}
\end{document}
