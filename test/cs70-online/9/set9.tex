% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode

% This is a simple template for a LaTeX document using the "article" class.
% See "book", "report", "letter" for other types of document.

\documentclass[11pt, preview]{standalone} % use larger type; default would be 10pt

\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)


\usepackage{../../markup}
%%% Examples of Article customizations
% These packages are optional, depending whether you want the features they provide.
% See the LaTeX Companion or other references for full information.

%%% PAGE DIMENSIONS
\usepackage{geometry} % to change the page dimensions
\geometry{a4paper} % or letterpaper (US) or a5paper or....
% \geometry{margin=2in} % for example, change the margins to 2 inches all round
% \geometry{landscape} % set up the page for landscape
%   read geometry.pdf for detailed page layout information

\usepackage{graphicx} % support the \includegraphics command and options
\usepackage{color, tikz}
% \usepackage[parfill]{parskip} % Activate to begin paragraphs with an empty line rather than an indent

%%% PACKAGES
\usepackage{amsmath, amsfonts,amssymb}
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{paralist} % very flexible & customisable lists (eg. enumerate/itemize, etc.)
\usepackage{verbatim} % adds environment for commenting out blocks of text & for better verbatim
\usepackage{subfig} % make it possible to include more than one captioned figure/table in a single float
% These packages are all incorporated in the memoir class to one degree or another...

%%% HEADERS & FOOTERS
\usepackage{fancyhdr} % This should be set AFTER setting up the page geometry
\pagestyle{fancy} % options: empty , plain , fancy
\renewcommand{\headrulewidth}{0pt} % customise the layout...
\lhead{}\chead{}\rhead{}
\lfoot{}\cfoot{\thepage}\rfoot{}

%%% SECTION TITLE APPEARANCE
\usepackage{sectsty}
\allsectionsfont{\sffamily\mdseries\upshape} % (See the fntguide.pdf for font help)
% (This matches ConTeXt defaults)

%%% ToC (table of contents) APPEARANCE
\usepackage[nottoc,notlof,notlot]{tocbibind} % Put the bibliography in the ToC
\usepackage[titles,subfigure]{tocloft} % Alter the style of the Table of Contents
\renewcommand{\cftsecfont}{\rmfamily\mdseries\upshape}
\renewcommand{\cftsecpagefont}{\rmfamily\mdseries\upshape} % No bold!

\newcommand{\N}{\mathbb{N}}
\newcommand{\Prob}{\mathbb{P}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\Q}{\mathbb{Q}}
%%% END Article customizations

%%% The "real" document content comes below...

\date{} % Activate to display a given date or no date (if empty),
         % otherwise the current date is printed 

\begin{document}
\config{name}{Independence, Hashing and Bin Packing}
\noindent{\bf Independence, Hashing and Bin Packing}.

\begin{enumerate}
\item {\bf Independence.} For each of the following examples, decide whether the listed events are mutually independent, pairwise independent, or neither (if the events are mutually independent, there is no need to also select pairwise independent).
\begin{enumerate}
\item The event of drawing a jack of hearts from the deck and the event of drawing a jack of clubs from the same deck. 
\begin{Multi}
Recall the definition of independence--is the probability of both events equal to the product of the probabilities of each event? Should one event influence the probability of the other?
\begin{itemize}
\FalseChoice\item Mutually Independent
\FalseChoice\item Only Pairwise Independent
\TrueChoice\item Neither
\Solution These events are not independent. The deck starts off with 52 cards, so $P(\text{Jack of Hearts})\ =\ P(\text{Jack of Clubs})\ =\ \frac{1}{52}$. Now, suppose we have drawn a jack of hearts already. We are left with 51 cards in the deck. Now $P(\text{Jack of clubs} \mid \text{Jack of hearts})\ =\ \frac{1}{51}$. Because knowing that we already drew a jack of hearts changed the probability that we will draw a jack of clubs, the events are dependent.
\end{itemize}
\end{Multi}
%-----------------------------------
\item The event of drawing a jack of hearts from the deck and the event of drawing an ace of diamonds from the same deck. 
\begin{Multi}
Recall the definition independence--is the probability of both events equal to the product of the probabilities of each event? Should one event influence the probability of the other?
\begin{itemize}
\FalseChoice\item Mutually Independent
\FalseChoice\item Only Pairwise Independent
\TrueChoice\item Neither
\Solution These events are not independent, by the same argument as above. The probability of drawing an ace of diamonds is initially $\frac{1}{52}$, while the probability of drawing an ace of diamons {\it given} you've already drawn a jack of hearts is $\frac{1}{51}$. Note that it doesn't matter which two cards we're considering! Because drawing some card $A$ changes the deck, the event of drawing some other card $B$ from the same deck is not independent of drawing $A$.
\end{itemize}
\end{Multi}
%-----------------------------------
\item The outcomes of three consecutive coinflips.
\begin{Multi}
Recall the definition of independence--is the probability of all three events equal to the product of the probabilities of each event? Should one event influence the probability of the others?
\begin{itemize}
\TrueChoice\item Mutually Independent
\FalseChoice\item Only Pairwise Independent
\FalseChoice\item Neither
\Solution The events are mutually independent. Knowing the value of any two coin flips does not affect the probability of the third landing on $H$.
\end{itemize}
\end{Multi}
%-----------------------------------
\item Given 2 random integers $x,y$, the event that $x = 5 \mod n$, the event that $y = 7 \mod n$, and the event that $x + y = 20 \mod n$. 
\begin{Multi}
Recall the definition of independence--is the probability of all events equal to the product of the probabilities of each event? Should one event influence the probability of the others?
\begin{itemize}
\FalseChoice\item Mutually Independent
\TrueChoice\item Only Pairwise Independent
\FalseChoice\item Neither
\Solution The events are pairwise independent. Call $A$ the event that $x = 5 \bmod n$, $B$ the event that $y = 7 \bmod n$ and $C$ the event that $x + y = 20 \bmod n$.\\ 

Because $x$ and $y$ are randomly generated integers, their value $\bmod n$ is equally likely to be anything from $0$ to $n - 1$. Because there are $n$ numbers in that range,

$$P(A)\ =\ \frac{1}{n}$$
$$P(B)\ =\ \frac{1}{n}$$

Now, because $x$ and $y$ are equally likely to be any number $\bmod n$, knowing nothing about $x$ and $y$ we can say nothing about their sum. It is also equally likely to take any value $\bmod n$:

$$P(C)\ =\ \frac{1}{n}$$

Consider the equation 

$$(x\ +\ y)\ \bmod n\ =\ c \bmod n$$

We've determined that all three values, $x \bmod n$, $y \bmod n$, and $c \bmod n$, are equally likely to take any value from $0$ to $n - 1$. What happens if we fix $x \bmod n\ =\ 5$? We get

$$(5\ +\ y)\ \bmod n\ =\ c \bmod n$$

This places no constraints on the possible values of $y$ and $c$, so $P(B)$ and $P(C)$ are unaffected. The same would be true if we fixed any one of the three terms and left the other two free. This shows {\it pairwise} independence - knowing the value of any one of the variables gives us no information about any other one.\\

Now, what happens if we fix {\it two} terms? Let's fix $x \bmod n\ =\ 5$ and $y \bmod n\ =\ 7$. The equation then becomes 

$$(5\ +\ 7) \bmod n\ =\ c \bmod n$$

Now we know that $c \equiv 12 \bmod n$, so $P(C)\ =\ P(c = 20 \bmod n)$ is no longer $\frac{1}{n}$. It is simply $1$ if $12 \equiv 20 \bmod n$, and $0$ if not. Fixing any two of the other variales would also fully determine the third one. This shows that the three events are not {\it mutually} independent.

\end{itemize}
\end{Multi}
%-----------------------------------
%\item Given 3 random integers $x,y,z$ such that $x + y + z = 0 \mod 2$, the event that $x = 1 \mod 2$, the event that $y = 0 \mod 2$, and the event that $z = 0 \mod 2$. 
%\begin{Multi}
%Recall the definition of independence--is the probability of all events equal to the product of the probabilities of each event? Should one event influence the probability of the others?
%\begin{itemize}
%\FalseChoice\item Mutually Independent
%\TrueChoice\item Only Pairwise Independent
%\FalseChoice\item Neither
%\end{itemize}
%\end{Multi}
%-----------------------------------
\end{enumerate}
%------------------------------------------------------------------------------------------------------------------------
\item {\bf The Principle of Inclusion-Exclusion.} The Principle of Inclusion-Exclusion states that for events $A_1, \ldots, A_n$ in probability space $S$,
\[
\Prob\left[\bigcup_{i = 1}^n A_i\right]\ =\ \sum_{i = 1}^n \Prob[A_i]\ -\ \sum_{\{i,j\}} \Prob[A_i\, \cap\, A_j]\ +\ \sum_{\{i,j,k\}} \Prob[A_i\, \cap\, A_j\, \cap A_k] - \cdots + (-1)^{n-1}\Prob\left[\bigcap_{i=1}^n\, A_i\right].
\]
That is, the probability of the union of events is the sum of the probabilities, minus the sum of the pairwise intersections (which were counted twice), plus the sum of the 3-way intersections (which were subtracted with the pairwise intersections), etc.

Consider the picture below--this depicts a probability space $S$, where each of the small stars is one outcome. 
\begin{center}
\includegraphics[width=7cm]{inclexcl.pdf}
\end{center}
\begin{enumerate}
\item How many outcomes are there in $S$ in total?
\begin{Freeform}{9}
\# outcomes = 
\Hint Each outcome is represented as a star--just count the number of stars.
\Solution There are $9$ stars total, meaning $9$ outcomes.
\end{Freeform}
%-----------------------------------
\item How many outcomes are in the union $A \cup B \cup C$?
\begin{Freeform}{6}
\# outcomes = 
\Hint Each outcome is represented as a star--just count the number of stars in $A,B$, and $C$.
\Solution Making sure to count each only once, we find $6$ in $A$, $B$, or $C$.
\end{Freeform}
%-----------------------------------
\item What is $\Prob[A \cup B \cup C]$? Please enter your answer as a fully reduced fraction (i.e. in the form $x/y$ where $x,y$ are the smallest possible integers they can be).
\begin{Freeform}{2/3}
$\Prob[A \cup B \cup C]$ = 
\Hint Review the definition of probability in a discrete space.
\Solution There are $9$ possible outcomes and $6$ in the event of interest, meaning the probability is $\frac{6}{9}\ =\ \frac{2}{3}$
\end{Freeform}
%-----------------------------------
\item Now, we will calculate this probability using the Principle of Inclusion-Exclusion. First, what is $ \Prob[A] + \Prob[B] + \Prob[C]$? Please enter your answer as a fully reduced fraction (i.e. in the form $x/y$ where $x,y$ are the smallest possible integers they can be).
\begin{Freeform}{10/9}
$ \Prob[A] + \Prob[B] + \Prob[C]$ = 
\Hint Review the definition of probability in a discrete space--how many outcomes fall in each of the events $A, B, C$? Take a sum of each of the individual probabilities.
\Solution There are $3$ outcomes in $A$, so $\Prob[A]\ =\ \frac{3}{9}\ =\ \frac{1}{3}$. There are $4$ outcomes in $B$, so $\Prob[B]\ =\ \frac{4}{9}$. There are $3$ outcomes in $C$, so $\Prob[C]\ =\ \frac{1}{3}$. Then 

$$\Prob[A]\ +\ \Prob[B]\ +\ \Prob[C]\ =\ \frac{1}{3}\ +\ \frac{4}{9}\ +\ \frac{1}{3}\ =\ \frac{10}{9}$$.

Notice how because of the overlap in events, these probabilities sum up to more than $1$.
\end{Freeform}
%-----------------------------------
\item Notice that the above was a severe overestimate of $\Prob[A \cup B \cup C]$. This is because all of the two-way intersections were counted twice, once for each individual event. 

Now, calculate the sum of probabilities of the intersections, $\Prob[A \cap B] + \Prob[B \cap C] + \Prob[C \cap A]$. Please enter your answer as a fully reduced fraction (i.e. in the form $x/y$ where $x,y$ are the smallest possible integers they can be).
\begin{Freeform}{5/9}
$\Prob[A \cap B] + \Prob[B \cap C] + \Prob[C \cap A]$ = 
\Hint Review the definition of probability in a discrete space--how many outcomes fall in each of the two-way intersections of the events? Take a sum of each of the individual probabilities.
\Solution There are two events in $A \cap B$, two events in $B \cap C$, and one event in $A \cap C$, giving $\Prob[A \cap B]\ =\ \frac{2}{9}$, $\Prob[B \cap C]\ =\ \frac{2}{9}$, and $\Prob[A \cap C]\ =\ \frac{1}{9}$. Summing, we get 

$$\Prob[A \cap B]\ +\ \Prob[B \cap C]\ +\ \Prob[A \cap C]\ =\ \frac{2}{9}\ +\ \frac{1}{9}\ +\ \frac{2}{9}\ =\ \frac{5}{9}$$
\end{Freeform}
%-----------------------------------
\item Now, subtract the two-way intersections from the sum of the individual probabilities. What is $\Prob[A] + \Prob[B] + \Prob[C] - (\Prob[A \cap B] + \Prob[B \cap C] + \Prob[C \cap A])$?. Please enter your answer as a fully reduced fraction (i.e. in the form $x/y$ where $x,y$ are the smallest possible integers they can be).
\begin{Freeform}{5/9}
$\Prob[A] + \Prob[B] + \Prob[C] - (\Prob[A \cap B] + \Prob[B \cap C] + \Prob[C \cap A])$ = 
\Hint Simply subtract your answer from part (e) from your answer in part (d), and ensure that your input is of the proper form.
\Solution Subtracting answers from (e) and (d), we get 

$$\frac{10}{9}\ -\ \frac{5}{9}\ =\ \frac{5}{9}$$
\end{Freeform}
%-----------------------------------
\item Finally, notice that this probability is slightly less than $\Prob[A \cup B \cup C]$. This is because the 3-way intersection was originally added 3 times, but then subtracted 3 times, which is once too many. 

What is $\Prob[A \cap B \cap C]$? Please enter your answer as a fully reduced fraction (i.e. in the form $x/y$ where $x,y$ are the smallest possible integers they can be).
\begin{Freeform}{1/9}
$\Prob[A \cap B \cap C]$ = 
\Hint Review the definition of probability in a discrete space--how many outcomes fall in $A \cap B \cap C$?
\Solution There is one star inside all three of $A$, $B$, and $C$, so $\Prob[A \cap B \cap C]\ =\ \frac{1}{9}$
\end{Freeform}
%-----------------------------------
\item Now, add back $\Prob[A \cap B \cap C]$ to your answer in the part (f). Does this probability match the probability you calculated in part (c)?
\begin{Multi}
\begin{itemize}
\TrueChoice\item Yes
\FalseChoice\item No
\Solution Adding part (g) to part (f), we have 

$$\frac{5}{9}\ +\ \frac{1}{9}\ =\ \frac{6}{9}$$

which should be the same answer you calculated in part (c)
\end{itemize}
\end{Multi}
\end{enumerate}

%------------------------------------------------------------------------------------------------------------------------

\item {\bf Balls and Bins.} Suppose you have $m$ labeled balls $a_1, \ldots, a_m$ that you have thrown one by one, uniformly at random into $n$ labeled bins $b_1, \ldots, b_n$. For each of the probabilities below, select all answers that apply from the list of choices.
\begin{enumerate}
\item What is the probability that $b_j$ contains $a_i$? Select all answers that apply.
\begin{Choices}
\Hint When $a_i$ is being thrown, what is the probability that it lands in $b_j$? How many choices are there?
\begin{enumerate}
\FalseChoice\item $\left(1 -\frac{1}{n}\right)^m$
\TrueChoice\item $\frac{1}{n}$
\FalseChoice\item $\frac{1}{m}$
\FalseChoice\item $\frac{m}{n}$
\Solution The probability that $b_j$ contains $a_i$ is the same as the probability that $a_i$ was thrown into $b_j$. The second formulation might make it a little easier to intuit the answer. A given ball $a_i$ has $n$ bins it could be thrown into uniformly at random. Since $b_j$ is one of those bins, the probability that $a_i$ is thrown into $b_j$ is $\frac{1}{n}$.
\end{enumerate}
\end{Choices}
%-----------------------------------
\item What is the probability that $b_j$ is empty? Select all answers that apply.
\begin{Multi}
\Hint For each ball, what is the probability that it does not end up in $b_j$? Are these events independent?
\begin{enumerate}
\TrueChoice\item $\left(1 -\frac{1}{n}\right)^m$
\TrueChoice\item $\binom{m}{0}\cdot\left(1 -\frac{1}{n}\right)^m$
\FalseChoice\item $\frac{1}{m!}$
\FalseChoice\item $\frac{1}{n!}$
\Solution For $b_j$ to be empty, no $a_i$ should have landed in it. This probability can be expressed as 

$$P(b_j \text{ empty})\ =\ P\left(\bigcap_{i = 1}^{m}\, a_i \text{ doesn't land in } b_j \right)$$

Where each ball lands is independent of where every other ball lands, so this can be expressed as 

$$P(b_j \text{ empty})\ =\ \prod_{i = 1}^{m}\, P(a_i \text{ doesn't land in } b_j)$$

We know from (a) that $P(a_i \text{ lands in } b_j) = \frac{1}{n}$, so $P(a_i \text{ doesn't land in } b_j)\ =\ 1 - \frac{1}{n}$. Plugging this in, we get 

$$P(b_j \text{ empty})\ =\ \prod_{i = 1}^{m}\, 1 - \frac{1}{n}\ =\ \left(1 - \frac{1}{n}\right)^m$$ 

Note that $\binom{m}{0} = 1$.\\
\end{enumerate}
\end{Multi}
%-----------------------------------
\item What is the probability that $b_j$ contains all of the balls? Select all answers that apply.
\begin{Choices}
For each ball, what is the probability that it ends up in $b_j$? Are these events independent?
\begin{enumerate}
\FalseChoice\item $\left(1-\frac{1}{n}\right)^m$
\TrueChoice\item $\left(\frac{1}{n}\right)^m$
\TrueChoice\item $\binom{m}{m}\cdot\left(\frac{1}{n}\right)^m$
\FalseChoice\item $\frac{1}{m!}$
\Solution The probability that $b_j$ contains all the balls can be expressed as 

$$P(b_j \text{ full})\ =\ P\left(\bigcap_{i = 1}^{m}\, a_i \text{ lands in } b_j \right)$$

Where each ball lands is independent of where every other ball lands, so this can be expressed as 

$$P(b_j \text{ full})\ =\ \prod_{i = 1}^{m}\, P(a_i \text{ lands in } b_j)$$

We know from (a) that $P(a_i \text{ lands in } b_j) = \frac{1}{n}$, so this becomes 

$$P(b_j \text{ full})\ =\ \prod_{i = 1}^{m}\, \frac{1}{n}\ =\ \left(\frac{1}{n}\right)^{m}$$

Note that $\binom{m}{m} = 1$.\\

\end{enumerate}
\end{Choices}
%-----------------------------------
\item What is the probability that $b_j$ contains exactly $k$ balls? Select all answers that apply.
\begin{Choices}
For each ball, what is the probability that it ends up in $b_j$? Are these events independent? Does order matter?
\begin{enumerate}
\FalseChoice\item $\binom{m}{k}\cdot\left(\frac{1}{n}\right)^{m-k}\cdot \left(1-\frac{1}{n}\right)^{k}$
\FalseChoice\item $\left(\frac{1}{n}\right)^{k}\cdot \left(1-\frac{1}{n}\right)^{m-k}$
\TrueChoice\item $\binom{m}{k}\cdot\left(\frac{1}{n}\right)^{k}\cdot \left(1-\frac{1}{n}\right)^{m-k}$
\FalseChoice\item $\left(\frac{1}{n}\right)^{k}$
\Solution How can we describe the number of balls in a bin? We know the balls are independent, and each lands in the bin with probability $\frac{1}{n}$. There are up to $m$ balls that could land. Answering the question of how many balls are in a bin becomes the same as answering the question of how many heads we would get if we tossed $m$ coins of bias $\frac{1}{n}$. Let $N_j$ be the number of balls in $b_j$.From the binomial distribution, we know

$$P(N_j = k)\ =\ \binom{m}{k} \left(\frac{1}{n}\right)^{k}\left(1 - \frac{1}{n}\right)^{n - k}$$

Notice that this generalization is consistent with our answers for (b) ($k = 0$) and (c) ($k = m$).
\end{enumerate}
\end{Choices}
%-----------------------------------
\item What is the probability that $b_j$ contains at most $k$ balls?
\begin{Multi}
What is the probability that $b_j$ contains exactly $i$ balls? Review the probability of a union of disjoint events in note 10.
\begin{enumerate}
\FalseChoice\item $\sum_{i = 0}^{k}\binom{m}{i}\cdot\left(\frac{1}{n}\right)^{m-i}\cdot \left(1-\frac{1}{n}\right)^{i}$
\TrueChoice\item $\sum_{i = 0}^{k} \binom{m}{i}\cdot\left(\frac{1}{n}\right)^{i}\cdot \left(1-\frac{1}{n}\right)^{m-i}$
\TrueChoice\item $1 - \sum_{i = k+1}^{m}\binom{m}{i}\cdot\left(\frac{1}{n}\right)^{i}\cdot \left(1-\frac{1}{n}\right)^{m-i}$
\TrueChoice\item $1 - \sum_{i = 0}^{m-k}\binom{m}{i}\cdot\left(\frac{1}{n}\right)^{m-i}\cdot \left(1-\frac{1}{n}\right)^{i}$
\Solution The last three choices are all correct. Let's look at each in turn and consider what it says in English. Consider

$$\sum_{i = 0}^{k} \binom{m}{i}\cdot\left(\frac{1}{n}\right)^{i}\cdot \left(1-\frac{1}{n}\right)^{m-i}$$

Each term in the summation is the probability that $b_j$ contains exactly $i$ balls. We are simply adding up the probability that it has $0$, $1$, $2$ ... up to $k$ balls. Because each of these events are mutually exclusive, we can simply add the probabilities to get the probability of their union.\\

Now consider

$$1 - \sum_{i = k+1}^{m}\binom{m}{i}\cdot\left(\frac{1}{n}\right)^{i}\cdot \left(1-\frac{1}{n}\right)^{m-i}$$

Again, each term in the summation is the probability that $b_j$ contains exactly $i$ balls. This time, however, we are summing up the probability of having $k + 1$, $k + 2$, ... up to $m$ balls. This gives us the probability that $b_j$ contains more than $k$ balls, and we take the complement to get the probability that $b_j$ contains at most $k$ balls.\\

Now consider 

$$1 - \sum_{i = 0}^{m-k}\binom{m}{i}\cdot\left(\frac{1}{n}\right)^{m-i}\cdot \left(1-\frac{1}{n}\right)^{i}$$

The term in the summation has changed; it now describes the probability that $b_j$ contains exactly $m - i$ balls. When we sum over $i$ from $0$ to $m - k$, we are summing the probability of having $m - 0$, $m - 1$, ... $m - (m - k)$ balls - in other words, form $m$ balls to $k$ balls in reverse order. This is equivalent to the second summation.\\

Note that the first choice ommitted the $1 - $. This calculates the probability that $b_j$ has more than $k$ balls.
\end{enumerate}
\end{Multi}
\end{enumerate}
%-----------------------------------
\item {\bf Processes, servers and overloading.} I have $M$ processes (jobs) and 
    $N$ servers that I can assign the jobs to. Any job may be assigned to any 
    server. Suppose I assign each job to a randomly chosen server, with all 
    servers being equally likely. We say that a server is overloaded if it is 
    assigned greater than or equal to $K$ jobs, where $K \leq M$. What is the 
    probability that the first server is overloaded?
\begin{Choices}
\begin{enumerate}
    \FalseChoice\item $\sum_{i=0}^{M-K-1} \dbinom{M}{K+i} \frac{(N-1)^{M-K-i}}{N^{M}}$
    \FalseChoice\item $\sum_{i=0}^{N-K} \dbinom{N}{K+i} \frac{(M-1)^{N-K-i}}{M^{N}}$
    \TrueChoice\item $\sum_{i=0}^{M-K} \dbinom{M}{K+i} \frac{(N-1)^{M-K-i}}{N^{M}}$
    \FalseChoice\item $\dbinom{M}{K} \frac{(N-1)^{M-K}}{N^{M}}$
    \FalseChoice\item $\left(\frac{N-1}{N}\right)^{M-K}$
    \Solution From above, we know the probability that a server has exactly $j$ jobs is 

    $$\binom{M}{j}\, \left(\frac{1}{N}\right)^{j}\left(1 - \frac{1}{N}\right)^{M - j}$$

    We can rearrange this to get 

    $$\binom{M}{j}\, \frac{1}{N^j}\left(\frac{N - 1}{N}\right)^{M - j}\ =\ \binom{M}{j}\, \frac{(N - 1)^{M - j}}{N^M}$$

    The probability that a server is overloaded is the sum of the probabiltiy that it has $K$ jobs, $K + 1$ jobs, ... up to $M$ jobs. This is given by equation (c).

\end{enumerate}
\end{Choices}
%-----------------------------------
\item {\bf Jobs and servers, without immediate repetition.} I have $M\geq 2$ 
    jobs and $N \geq M$ servers to assign the jobs to.
    
    I use the following system to assign the jobs: the 
    first job is randomly assigned to one of the $N$ servers (with all servers 
    being equally likely). The second job is again assigned randomly to a 
    server, except that the server that got the first job is excluded from the 
    selection (any of the other $N-1$ servers are equally likely to get the job). 
    Similarly, the third job is assigned randomly, except this time, the 
    server that got the second job is excluded (any of the other $N-1$ servers 
    have equal likelihood of being chosen for this job). And so on until all 
    $M$ jobs have been assigned.

    What is the probability that each of the $M$ jobs goes to a different 
    server?
\begin{Choices}
\begin{enumerate}
    \FalseChoice\item $\sum_{i=0}^{N-M}\frac{(N-i-2)!}{(N-M-i)! (N-1)^{M-2}}$
    \FalseChoice\item $\frac{(M-2)!}{(N-M)! (M-2)^{N-1}}$
    \FalseChoice\item $\frac{(N-1)!}{(N-M)! N^{M-1}}$
    \FalseChoice\item $\frac{(N-2)!}{(N-M)! (N-2)^{M-3}}$
    \TrueChoice\item $\frac{(N-2)!}{(N-M)! (N-1)^{M-2}}$
    \Solution The answer is (e). First, let's figure out how many ways there are to assign jobs to servers.\\

    For the first job, we have $N$ choices. For each of the next $M - 1$ jobs, we have $N - 1$ choices, meaning the number of ways to assign jobs is $N(N - 1)^{M - 1}$.\\

    Now, how many ways are there to assign each job to a unique server? For the first job, we have $N$ choices. For the second, we have $N - 1$, for the third we have $N - 2$, and so on. This means the number of ways to uniquely assign jobs is $N!$.\\

    The probability of assigning each job to a different server is then

    $$\frac{N!}{N(N - 1)^{M - 1}}\ =\ \frac{(N - 2)!}{(N - 1)^{M - 2}}$$
\end{enumerate}
\end{Choices}
%-----------------------------------
\item {\bf Bounding overload probabilities.} I have a load balancing set up 
    with $N$ servers, where I can guarantee that the probability of the $i^{th}$ 
    server being overloaded (where $1\leq i \leq N$) is exactly $p$, which is 
    independent of $i$. Which of the following is true? Check all that apply.
    
   \begin{Multi}
\begin{enumerate}
    \FalseChoice\item $\textnormal{Pr (no server is overloaded)} \leq 1-Np$
    \TrueChoice\item $\textnormal{Pr (no server is overloaded)} \geq 1-Np$
    \TrueChoice\item $\textnormal{Pr (at least one server is overloaded)} \geq p$
    \FalseChoice\item $\textnormal{Pr (no server is overloaded)} \leq 1-N(1-p)$
    \FalseChoice\item $\textnormal{Pr (no server is overloaded)} \geq 1-(1-p)^{2N}$
    \TrueChoice\item $\textnormal{Pr (at least one server is overloaded)} \leq Np$
    \Solution The correct choices are (b), (c), and (f).

    Let's consider (b). Observe that $P(\text{No server is overloaded})\ =\ 1 - P(\text{At least one server is overloaded})$. At least one server being overloaded means server $1$ was overloaded, or server $2$ was overloaded, ... or server $m$ was overloaded. So this expression translates to 

    $$P(\text{No server is overloaded})\ =\ 1 -\ P\left(\bigcup_{i = 1}^{N}\, \text{Server $i$ is overloaded}\right)$$

    We know $P(\text{Server $i$ is overloaded})\ =\ p$. By this and the union bound, we get 

    $$P(\text{At least one server is overloaded})\ \leq\ \sum_{i = 1}^{N}\, p\ =\ Np$$

    Because this is an {\it upper} bound, when we take its complement we find a {\it lower} bound for the complement, giving

    $$P(\text{No server is overloaded})\ \geq\ 1\ -\ Np$$

    This demonstrates why (a) is incorrect, as well as why (f) is correct.\\

    Now consider (c). Recall that $P(\text{At least one server is overloaded})$ is the union of the probality that each individual server is overloaded. What's the smallest value the union could take? Clearly the probability that server $1$ or is overloaded or server $2$ is overloaded or ... or server $N$ is overloaded can't be smaller than the probability that server $1$ by itself is overloaded, so the lower bound is $p$.

    \end{enumerate}
\end{Multi}

\end{enumerate}
\end{document}
