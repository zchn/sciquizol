import subprocess
import sys
import os
import wand.image
import re
import math
import json


import config
from util import myassert, shouldntbehere

CONFIGFILE = sys.argv[1]
config.initConfig(CONFIGFILE)

def load_annotations(output_dir):
    annot_file = os.path.join(output_dir, 'output.annotations')
    with open(annot_file, 'r') as f:
        annot_content = f.read()
    annot_list = annot_content.split('====== annotation ======\n')
    myassert(annot_list[0] == 'BEGIN ANNOTATIONS\n')
    annot_list = annot_list[1:]
    snippets = {}
    for annot in annot_list:
        if annot.startswith('====== jssnippet ======\n'):
            _, snippetid, _, snippetcontent, _, _ = annot.replace('\\%', '%').split('\n')
            snippets[snippetid] = snippetcontent
            print 'adding jssnipppet {}:{}'.format(snippetid, snippetcontent)
        else:
            myassert(False, 'unknown annotation: '+annot)
    return { 'jssnippets': snippets }

def compile(entry, outdir, tmpdir):
    entry_file = os.path.basename(entry)
    entry_directory = os.path.dirname(entry)
    directory = tmpdir
    retcode = subprocess.Popen(['pdflatex', '-interaction=batchmode', '-8bit', '-output-directory={}'.format(os.path.abspath(directory)), '-jobname=output', entry_file], stdout=None, stderr=None, cwd=entry_directory).wait()
    if retcode!=0:
        raise subprocess.CalledProcessError(retcode, 'pdflatex')
    with open(os.path.join(directory, 'output.aux')) as f:
        aux = f.read()
    annotations = load_annotations(directory)
    points = [(x, float(y)) for (x, y) in re.findall(r'\\zref\@newlabel\{([^\}]+)\}\{\\posy\{(\d+)\}\}', aux)]
    image = wand.image.Image(resolution=100, filename=os.path.join(directory, 'output.pdf'))
    y0 = points[0][1]
    y1 = points[-1][1]
    # Convert x,y numbers to a relative number of the image size
    points = [(x, int(math.ceil((image.size[1])*float(y0-y)/float(y0-y1)))) for x, y in points]
    config = [x[len('config='):] for x, _ in points if x.startswith('config=')]
    config = dict([(x.split('=')[0], '='.join(x.split('=')[1:])) for x in config])
    points = [(x, y) for x, y in points if not x.startswith('config=')]
    dname = entry_file
    if dname.rindex('.')!=-1:
        dname = dname[:dname.rindex('.')]
    dname = os.path.join('sets', dname)
    obj = {}
    os.system("rm -rf {}".format(dname))
    os.system("mkdir -p {}".format(dname))
    obj['name'] = config.get('name', entry_file)
    obj['contents'] = []
    points = [(x, y0, y1) for ((x, y0), (_, y1)) in zip(points[:-1], points[1:])]
    part = {}
    def push():
        if part.get('type') is not None:
            obj['contents'].append(part.copy())
        part.clear()
        part['imgs'] = []
    push()
    all_imgs = []
    for x, y0, y1 in points:
        def append(contenttype='text'):
            if y1!=y0:
                all_imgs.append('{}/{}.png'.format(dname, len(all_imgs)))
                img = image[:, y0:y1]
                img.format='png'
                img.save(filename=os.path.join(outdir, all_imgs[-1]))
                if contenttype == 'hint':
                    if part.get('hint') is not None:
                        raise Exception('Only one hint allowed for each question.')
                    part['hint'] = all_imgs[-1]
                elif contenttype == 'solution':
                    part['solution'] = all_imgs[-1]
                else:
                    part['imgs'].append(all_imgs[-1])
                print 'Appending {}: {}'.format(contenttype, all_imgs[-1])
        def hint():
            append('hint')
        def solution():
            append('solution')
        if x=='begin_document':
            push()
            part['type']='static'
            append()
        elif x=='begin_choices':
            push()
            part['type']='single'
            #hint()
        elif x=='begin_multi':
            push()
            part['type']='multi'
            part['answer'] = []
            #hint()
        elif x=='begin_freeform':
            push()
            part['type']='freeform'
            append()
        elif x=='begin_freeformjs':
            push()
            part['type']='freeformjs'
            append()
        elif x in ['end_choices', 'end_multi', 'end_freeform', 'end_freeformjs']:
            push()
            part['type']='static'
            append()
        elif x.startswith('ans_equals@'):
            append()
            part['answer'] = x[len('ans_equals@'):]
        elif x.startswith('ans_js@'):
            append()
            part['answer'] = annotations['jssnippets'][x[len('ans_js@'):]]
        elif x=='true_choice':
            if part['type']=='single':
                part['answer']=len(part['imgs'])
            elif part['type']=='multi':
                part['answer'].append(len(part['imgs']))
            append()
        elif x=='false_choice':
            append()
        elif x=='hint':
            hint()
        elif x=='solution':
            solution()
    push()
    with open(os.path.join(outdir, dname, 'meta.js'), 'w') as f:
        f.write('sets.push({})'.format(json.dumps(obj)))



for dirname in ['outdir','tmpdir']:
    if not os.path.exists(config.CONFIG[dirname]):
        os.system('mkdir "{}"'.format(config.CONFIG[dirname]))

for texfilepath in config.CONFIG['texfiles']:
    compile(texfilepath, config.CONFIG['outdir'], config.CONFIG['tmpdir'])

HTMLPATH = os.path.join(os.path.dirname(__file__), '../web/')
for filepath in ['index.html','home.html','set.html', 'js/xeditable.js', 'js/xeditable.min.js', 'css/xeditable.css']:
    os.system('cp "{}" "{}"'.format(os.path.join(HTMLPATH, filepath), config.CONFIG['outdir']))

