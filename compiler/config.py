import os
import json

from util import maybenormpath

CONFIGFILE = None
CONFIG = None 

def initConfig(configfile):
    global CONFIGFILE, CONFIG
    CONFIGFILE = configfile
    with open(CONFIGFILE, 'r') as f:
        CONFIG = json.loads(f.read())
    
    if 'workdir' not in CONFIG:
        CONFIG['workdir'] = os.path.dirname(os.path.abspath(CONFIGFILE))
    CONFIG['texfiles'] = map(lambda x: maybenormpath(x, CONFIG['workdir']), CONFIG['texfiles'])
    CONFIG['tmpdir'] = maybenormpath(CONFIG['tmpdir'], CONFIG['workdir'])
    CONFIG['outdir'] = maybenormpath(CONFIG['outdir'], CONFIG['workdir'])
    

