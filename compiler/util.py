import os

def myassert(condition=False,msg=""):
    if not condition:
        shouldntbehere(msg)

def shouldntbehere(msg=""):
    raise Exception("Shouldn't be here\n"+str(msg))

def maybenormpath(path, basepath):
    if not path.startswith('/'):
        return os.path.normpath(os.path.join(basepath, path))
    else:
        return path
