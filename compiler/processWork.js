'use strict';

var fs = require('fs');
var sys = require('sys');
var exec = require('child_process').exec;
var util = require('util');
function puts(error, stdout, stderr) { sys.puts(stdout); }


var workName = process.argv[2];
var pdfPath = workName + '.pdf';
var auxPath = workName + '.aux';
var annotPath = workName + '.annotations';
var imgDensity = 600;
var cmd = util.format('convert -density %d %s %s-img-%%d.png', imgDensity, pdfPath, workName);
util.log(cmd);
exec(cmd, puts);

function processAux(auxPath, outJson){
    var content = fs.readFileSync(auxPath).toString();
    console.log(content.split('\n'));
}

function processAnnotations(annotPath, outJson){
}

var mainJson = {
    'version':1,
    'pages':[],
    'problems':[]
};

processAux(auxPath, mainJson);
processAnnotations(annotPath, mainJson);




